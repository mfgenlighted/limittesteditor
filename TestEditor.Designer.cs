﻿namespace TestEditor
{
    partial class TestEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestEditor));
            this.label1 = new System.Windows.Forms.Label();
            this.lvAllProducts = new System.Windows.Forms.ListView();
            this.lvProductCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFunctioncode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvModDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bEdit = new System.Windows.Forms.Button();
            this.bNew = new System.Windows.Forms.Button();
            this.bGetCurrentLimits = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lEditingOn = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.serversToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serversToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.lbFilter = new System.Windows.Forms.Label();
            this.bView = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 52);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Current products";
            // 
            // lvAllProducts
            // 
            this.lvAllProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvProductCode,
            this.lvVersion,
            this.lvFunctioncode,
            this.lvModDate});
            this.lvAllProducts.FullRowSelect = true;
            this.lvAllProducts.GridLines = true;
            this.lvAllProducts.Location = new System.Drawing.Point(34, 77);
            this.lvAllProducts.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lvAllProducts.Name = "lvAllProducts";
            this.lvAllProducts.Size = new System.Drawing.Size(728, 596);
            this.lvAllProducts.TabIndex = 4;
            this.lvAllProducts.UseCompatibleStateImageBehavior = false;
            this.lvAllProducts.View = System.Windows.Forms.View.Details;
            // 
            // lvProductCode
            // 
            this.lvProductCode.Text = "Product Code";
            this.lvProductCode.Width = 86;
            // 
            // lvVersion
            // 
            this.lvVersion.Text = "Version";
            this.lvVersion.Width = 104;
            // 
            // lvFunctioncode
            // 
            this.lvFunctioncode.Text = "Function Code";
            this.lvFunctioncode.Width = 95;
            // 
            // lvModDate
            // 
            this.lvModDate.Text = "Modify Date";
            this.lvModDate.Width = 103;
            // 
            // bEdit
            // 
            this.bEdit.Location = new System.Drawing.Point(970, 149);
            this.bEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bEdit.Name = "bEdit";
            this.bEdit.Size = new System.Drawing.Size(112, 35);
            this.bEdit.TabIndex = 5;
            this.bEdit.Text = "Edit";
            this.bEdit.UseVisualStyleBackColor = true;
            this.bEdit.Click += new System.EventHandler(this.bEdit_Click);
            // 
            // bNew
            // 
            this.bNew.Location = new System.Drawing.Point(970, 194);
            this.bNew.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bNew.Name = "bNew";
            this.bNew.Size = new System.Drawing.Size(112, 35);
            this.bNew.TabIndex = 6;
            this.bNew.Text = "New";
            this.bNew.UseVisualStyleBackColor = true;
            this.bNew.Click += new System.EventHandler(this.bNew_Click);
            // 
            // bGetCurrentLimits
            // 
            this.bGetCurrentLimits.Location = new System.Drawing.Point(924, 320);
            this.bGetCurrentLimits.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bGetCurrentLimits.Name = "bGetCurrentLimits";
            this.bGetCurrentLimits.Size = new System.Drawing.Size(196, 35);
            this.bGetCurrentLimits.TabIndex = 7;
            this.bGetCurrentLimits.Text = "Get current limits";
            this.bGetCurrentLimits.UseVisualStyleBackColor = true;
            this.bGetCurrentLimits.Click += new System.EventHandler(this.bGetCurrentLimits_Click);
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(970, 240);
            this.bDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(112, 35);
            this.bDelete.TabIndex = 8;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 700);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Editing on:";
            // 
            // lEditingOn
            // 
            this.lEditingOn.AutoSize = true;
            this.lEditingOn.Location = new System.Drawing.Point(129, 700);
            this.lEditingOn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lEditingOn.Name = "lEditingOn";
            this.lEditingOn.Size = new System.Drawing.Size(51, 20);
            this.lEditingOn.TabIndex = 10;
            this.lEditingOn.Text = "label3";
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.serversToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1184, 35);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem1,
            this.exportLimitToolStripMenuItem,
            this.importLimitToolStripMenuItem,
            this.aboutToolStripMenuItem1});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(252, 30);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // exportLimitToolStripMenuItem
            // 
            this.exportLimitToolStripMenuItem.Name = "exportLimitToolStripMenuItem";
            this.exportLimitToolStripMenuItem.Size = new System.Drawing.Size(252, 30);
            this.exportLimitToolStripMenuItem.Text = "Export Limit";
            this.exportLimitToolStripMenuItem.Click += new System.EventHandler(this.exportDataToolStripMenuItem_Click);
            // 
            // importLimitToolStripMenuItem
            // 
            this.importLimitToolStripMenuItem.Name = "importLimitToolStripMenuItem";
            this.importLimitToolStripMenuItem.Size = new System.Drawing.Size(252, 30);
            this.importLimitToolStripMenuItem.Text = "Import Limit";
            this.importLimitToolStripMenuItem.Click += new System.EventHandler(this.importDataToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(252, 30);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // serversToolStripMenuItem1
            // 
            this.serversToolStripMenuItem1.Name = "serversToolStripMenuItem1";
            this.serversToolStripMenuItem1.Size = new System.Drawing.Size(81, 29);
            this.serversToolStripMenuItem1.Text = "Servers";
            this.serversToolStripMenuItem1.Click += new System.EventHandler(this.configureToolStripMenuItem_Click);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.importToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem,
            this.toolStripSeparator2,
            this.aboutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(151, 30);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportDataToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(151, 30);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importDataToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(148, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(151, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(148, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(151, 30);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // serversToolStripMenuItem
            // 
            this.serversToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureToolStripMenuItem});
            this.serversToolStripMenuItem.Name = "serversToolStripMenuItem";
            this.serversToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.serversToolStripMenuItem.Text = "Servers";
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            this.configureToolStripMenuItem.Size = new System.Drawing.Size(174, 30);
            this.configureToolStripMenuItem.Text = "Configure";
            this.configureToolStripMenuItem.Click += new System.EventHandler(this.configureToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(924, 398);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Filter:";
            // 
            // lbFilter
            // 
            this.lbFilter.AutoSize = true;
            this.lbFilter.Location = new System.Drawing.Point(981, 398);
            this.lbFilter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbFilter.Name = "lbFilter";
            this.lbFilter.Size = new System.Drawing.Size(45, 20);
            this.lbFilter.TabIndex = 13;
            this.lbFilter.Text = "none";
            // 
            // bView
            // 
            this.bView.Location = new System.Drawing.Point(970, 102);
            this.bView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bView.Name = "bView";
            this.bView.Size = new System.Drawing.Size(112, 35);
            this.bView.TabIndex = 14;
            this.bView.Text = "View";
            this.bView.UseVisualStyleBackColor = true;
            this.bView.Click += new System.EventHandler(this.bView_Click);
            // 
            // TestEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 760);
            this.Controls.Add(this.bView);
            this.Controls.Add(this.lbFilter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lEditingOn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bGetCurrentLimits);
            this.Controls.Add(this.bNew);
            this.Controls.Add(this.bEdit);
            this.Controls.Add(this.lvAllProducts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "TestEditor";
            this.Text = "LimitsEditor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvAllProducts;
        private System.Windows.Forms.ColumnHeader lvProductCode;
        private System.Windows.Forms.ColumnHeader lvVersion;
        private System.Windows.Forms.Button bEdit;
        private System.Windows.Forms.Button bNew;
        private System.Windows.Forms.Button bGetCurrentLimits;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lEditingOn;
        private System.Windows.Forms.ColumnHeader lvFunctioncode;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serversToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbFilter;
        private System.Windows.Forms.ColumnHeader lvModDate;
        private System.Windows.Forms.Button bView;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem serversToolStripMenuItem1;
    }
}