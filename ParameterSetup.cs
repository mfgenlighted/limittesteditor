﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class ParameterSetup : Form
    {
        public List<LimitsData.parametersItem> ParamList = new List<LimitsData.parametersItem>();

        private bool itemAdd = true;
        bool optionalPassThru = true;

        public ParameterSetup()
        {
            InitializeComponent();
            optionalPassThru = true;        // any parameter created is optional
        }

        public ParameterSetup(LimitsData.parametersItem param)
        {
            InitializeComponent();
            tbParameter.Text = param.name;
            tbData.Text = param.value;
            optionalPassThru = param.optional;
            itemAdd = false;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            LimitsData.parametersItem paramitem = new LimitsData.parametersItem();

            paramitem.name = tbParameter.Text;
            paramitem.value = tbData.Text;
            ParamList.Add(paramitem);
            if (itemAdd)
            {
                if (MessageBox.Show("Added another parameter?", "Exit", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    tbParameter.Text = string.Empty;
                    tbData.Text = string.Empty;
                    optionalPassThru = true;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
            else
                DialogResult = DialogResult.OK;
        }
    }
}
