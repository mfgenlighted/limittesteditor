﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEditor
{
    public partial class GetTestListFilter : Form
    {
        public string PCFilter;
        public string FunctionFilter;

        public GetTestListFilter(string currentPCFilter, string currentFunctionFilter)
        {
            InitializeComponent();
            tbFilter.Text = currentPCFilter;
            tbFunctionCode.Text = currentFunctionFilter;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PCFilter = tbFilter.Text;
            FunctionFilter = tbFunctionCode.Text;
        }
    }
}
