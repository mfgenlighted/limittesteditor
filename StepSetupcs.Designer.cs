﻿namespace TestEditor
{
    partial class StepSetupcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbActionOnFail = new System.Windows.Forms.ComboBox();
            this.cbActionOnPass = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbTestType = new System.Windows.Forms.ComboBox();
            this.nudMaxFails = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nudPassAfterxPasses = new System.Windows.Forms.NumericUpDown();
            this.bCancel = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.bAddParameters = new System.Windows.Forms.Button();
            this.bAddLimits = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lvParameter = new System.Windows.Forms.ListView();
            this.parameterName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.parameterData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLimits = new System.Windows.Forms.ListView();
            this.limitName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.limitValue1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.limitValue2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.limitOperation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bDeleteParameter = new System.Windows.Forms.Button();
            this.bDeleteLimit = new System.Windows.Forms.Button();
            this.cbStepOperation = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bEditParameter = new System.Windows.Forms.Button();
            this.bEditLimit = new System.Windows.Forms.Button();
            this.tbStepName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxFails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPassAfterxPasses)).BeginInit();
            this.SuspendLayout();
            // 
            // cbActionOnFail
            // 
            this.cbActionOnFail.FormattingEnabled = true;
            this.cbActionOnFail.Items.AddRange(new object[] {
            "NEXT",
            "END",
            "RETEST"});
            this.cbActionOnFail.Location = new System.Drawing.Point(61, 107);
            this.cbActionOnFail.Name = "cbActionOnFail";
            this.cbActionOnFail.Size = new System.Drawing.Size(77, 21);
            this.cbActionOnFail.TabIndex = 2;
            this.cbActionOnFail.Text = "END";
            // 
            // cbActionOnPass
            // 
            this.cbActionOnPass.FormattingEnabled = true;
            this.cbActionOnPass.Items.AddRange(new object[] {
            "NEXT",
            "END"});
            this.cbActionOnPass.Location = new System.Drawing.Point(158, 107);
            this.cbActionOnPass.Name = "cbActionOnPass";
            this.cbActionOnPass.Size = new System.Drawing.Size(77, 21);
            this.cbActionOnPass.TabIndex = 3;
            this.cbActionOnPass.Text = "NEXT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Test type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Action on Fail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Action on Pass";
            // 
            // cbTestType
            // 
            this.cbTestType.FormattingEnabled = true;
            this.cbTestType.Items.AddRange(new object[] {
            "RUN",
            "PASSFAIL",
            "LIMITS"});
            this.cbTestType.Location = new System.Drawing.Point(61, 43);
            this.cbTestType.Name = "cbTestType";
            this.cbTestType.Size = new System.Drawing.Size(77, 21);
            this.cbTestType.TabIndex = 0;
            this.cbTestType.Text = "RUN";
            // 
            // nudMaxFails
            // 
            this.nudMaxFails.Location = new System.Drawing.Point(64, 164);
            this.nudMaxFails.Name = "nudMaxFails";
            this.nudMaxFails.Size = new System.Drawing.Size(68, 20);
            this.nudMaxFails.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(67, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Max fails";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(170, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Pass after x passes";
            // 
            // nudPassAfterxPasses
            // 
            this.nudPassAfterxPasses.Location = new System.Drawing.Point(167, 164);
            this.nudPassAfterxPasses.Name = "nudPassAfterxPasses";
            this.nudPassAfterxPasses.Size = new System.Drawing.Size(68, 20);
            this.nudPassAfterxPasses.TabIndex = 5;
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(973, 445);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 7;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(1073, 445);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 6;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bAddParameters
            // 
            this.bAddParameters.Location = new System.Drawing.Point(356, 373);
            this.bAddParameters.Name = "bAddParameters";
            this.bAddParameters.Size = new System.Drawing.Size(105, 23);
            this.bAddParameters.TabIndex = 8;
            this.bAddParameters.Text = "Add Parameters";
            this.bAddParameters.UseVisualStyleBackColor = true;
            this.bAddParameters.Click += new System.EventHandler(this.bAddParameters_Click);
            // 
            // bAddLimits
            // 
            this.bAddLimits.Location = new System.Drawing.Point(750, 373);
            this.bAddLimits.Name = "bAddLimits";
            this.bAddLimits.Size = new System.Drawing.Size(105, 23);
            this.bAddLimits.TabIndex = 9;
            this.bAddLimits.Text = "Add Limits";
            this.bAddLimits.UseVisualStyleBackColor = true;
            this.bAddLimits.Click += new System.EventHandler(this.bAddLimits_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(359, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(752, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Limits";
            // 
            // lvParameter
            // 
            this.lvParameter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.parameterName,
            this.parameterData});
            this.lvParameter.GridLines = true;
            this.lvParameter.Location = new System.Drawing.Point(362, 123);
            this.lvParameter.Name = "lvParameter";
            this.lvParameter.Size = new System.Drawing.Size(318, 230);
            this.lvParameter.TabIndex = 12;
            this.lvParameter.UseCompatibleStateImageBehavior = false;
            this.lvParameter.View = System.Windows.Forms.View.Details;
            // 
            // parameterName
            // 
            this.parameterName.Text = "Name";
            this.parameterName.Width = 107;
            // 
            // parameterData
            // 
            this.parameterData.Text = "Data";
            this.parameterData.Width = 203;
            // 
            // lvLimits
            // 
            this.lvLimits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.limitName,
            this.limitValue1,
            this.limitValue2,
            this.limitOperation});
            this.lvLimits.GridLines = true;
            this.lvLimits.Location = new System.Drawing.Point(755, 123);
            this.lvLimits.Name = "lvLimits";
            this.lvLimits.Size = new System.Drawing.Size(377, 230);
            this.lvLimits.TabIndex = 13;
            this.lvLimits.UseCompatibleStateImageBehavior = false;
            this.lvLimits.View = System.Windows.Forms.View.Details;
            // 
            // limitName
            // 
            this.limitName.Text = "Name";
            this.limitName.Width = 107;
            // 
            // limitValue1
            // 
            this.limitValue1.Text = "Value1";
            this.limitValue1.Width = 93;
            // 
            // limitValue2
            // 
            this.limitValue2.Text = "Value2";
            this.limitValue2.Width = 109;
            // 
            // limitOperation
            // 
            this.limitOperation.Text = "Operation";
            // 
            // bDeleteParameter
            // 
            this.bDeleteParameter.Location = new System.Drawing.Point(477, 373);
            this.bDeleteParameter.Name = "bDeleteParameter";
            this.bDeleteParameter.Size = new System.Drawing.Size(99, 23);
            this.bDeleteParameter.TabIndex = 10;
            this.bDeleteParameter.Text = "Delete Parameter";
            this.bDeleteParameter.UseVisualStyleBackColor = true;
            this.bDeleteParameter.Click += new System.EventHandler(this.bDeleteParameter_Click);
            // 
            // bDeleteLimit
            // 
            this.bDeleteLimit.Location = new System.Drawing.Point(861, 373);
            this.bDeleteLimit.Name = "bDeleteLimit";
            this.bDeleteLimit.Size = new System.Drawing.Size(99, 23);
            this.bDeleteLimit.TabIndex = 11;
            this.bDeleteLimit.Text = "Delete Limit";
            this.bDeleteLimit.UseVisualStyleBackColor = true;
            this.bDeleteLimit.Click += new System.EventHandler(this.bDeleteLimit_Click);
            // 
            // cbStepOperation
            // 
            this.cbStepOperation.FormattingEnabled = true;
            this.cbStepOperation.Items.AddRange(new object[] {
            "RUN",
            "SKIP",
            "PASS",
            "FAIL",
            "GOTO"});
            this.cbStepOperation.Location = new System.Drawing.Point(61, 226);
            this.cbStepOperation.Name = "cbStepOperation";
            this.cbStepOperation.Size = new System.Drawing.Size(77, 21);
            this.cbStepOperation.TabIndex = 20;
            this.cbStepOperation.Text = "RUN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(61, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Step Operation";
            // 
            // bEditParameter
            // 
            this.bEditParameter.Location = new System.Drawing.Point(592, 373);
            this.bEditParameter.Name = "bEditParameter";
            this.bEditParameter.Size = new System.Drawing.Size(99, 23);
            this.bEditParameter.TabIndex = 22;
            this.bEditParameter.Text = "Edit Parameter";
            this.bEditParameter.UseVisualStyleBackColor = true;
            this.bEditParameter.Click += new System.EventHandler(this.bEditParameter_Click);
            // 
            // bEditLimit
            // 
            this.bEditLimit.Location = new System.Drawing.Point(969, 373);
            this.bEditLimit.Name = "bEditLimit";
            this.bEditLimit.Size = new System.Drawing.Size(99, 23);
            this.bEditLimit.TabIndex = 23;
            this.bEditLimit.Text = "Edit limit";
            this.bEditLimit.UseVisualStyleBackColor = true;
            this.bEditLimit.Click += new System.EventHandler(this.bEditLimit_Click);
            // 
            // tbStepName
            // 
            this.tbStepName.Location = new System.Drawing.Point(362, 44);
            this.tbStepName.Name = "tbStepName";
            this.tbStepName.Size = new System.Drawing.Size(375, 20);
            this.tbStepName.TabIndex = 24;
            this.tbStepName.TextChanged += new System.EventHandler(this.tbStepName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(362, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Test name";
            // 
            // StepSetupcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 505);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbStepName);
            this.Controls.Add(this.bEditLimit);
            this.Controls.Add(this.bEditParameter);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbStepOperation);
            this.Controls.Add(this.bDeleteLimit);
            this.Controls.Add(this.bDeleteParameter);
            this.Controls.Add(this.lvLimits);
            this.Controls.Add(this.lvParameter);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.bAddLimits);
            this.Controls.Add(this.bAddParameters);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nudPassAfterxPasses);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudMaxFails);
            this.Controls.Add(this.cbTestType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbActionOnPass);
            this.Controls.Add(this.cbActionOnFail);
            this.Name = "StepSetupcs";
            this.Text = "Step Setup";
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxFails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPassAfterxPasses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbActionOnFail;
        private System.Windows.Forms.ComboBox cbActionOnPass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbTestType;
        private System.Windows.Forms.NumericUpDown nudMaxFails;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudPassAfterxPasses;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bAddParameters;
        private System.Windows.Forms.Button bAddLimits;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView lvParameter;
        private System.Windows.Forms.ColumnHeader parameterName;
        private System.Windows.Forms.ColumnHeader parameterData;
        private System.Windows.Forms.ListView lvLimits;
        private System.Windows.Forms.ColumnHeader limitName;
        private System.Windows.Forms.ColumnHeader limitValue1;
        private System.Windows.Forms.ColumnHeader limitValue2;
        private System.Windows.Forms.Button bDeleteParameter;
        private System.Windows.Forms.Button bDeleteLimit;
        private System.Windows.Forms.ComboBox cbStepOperation;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bEditParameter;
        private System.Windows.Forms.Button bEditLimit;
        private System.Windows.Forms.ColumnHeader limitOperation;
        private System.Windows.Forms.TextBox tbStepName;
        private System.Windows.Forms.Label label4;
    }
}