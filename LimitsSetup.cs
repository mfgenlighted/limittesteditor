﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class LimitsSetup : Form
    {
        public List<LimitsData.limitsItem> LimitList = new List<LimitsData.limitsItem>();

        private bool itemAdd = true;
        private bool optionalPassthru = true;
        public LimitsSetup()
        {
            InitializeComponent();
            optionalPassthru = true;        // any item added is optional
        }

        public LimitsSetup(LimitsData.limitsItem limit)
        {
            InitializeComponent();
            tbName.Text = limit.name;
            tbName.Enabled = false;         // can not change the name
            tbType.Text = limit.type;
            tbOperation.Text = limit.operation;
            tbValue1.Text = limit.value1;
            tbValue2.Text = limit.value2;
            optionalPassthru = limit.optional;
        }


        private void bSave_Click(object sender, EventArgs e)
        {
            LimitsData.limitsItem limititem = new LimitsData.limitsItem();

            limititem.name = tbName.Text;
            limititem.type = tbType.Text;
            limititem.operation = tbOperation.Text;
            limititem.value1 = tbValue1.Text;
            limititem.value2 = tbValue2.Text;
            limititem.optional = optionalPassthru;
            LimitList.Add(limititem);
            if (itemAdd)
            {
                if (MessageBox.Show("Added another limit?", "Exit", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    tbName.Text = string.Empty;
                    tbType.Text = string.Empty;
                    tbOperation.Text = string.Empty;
                    tbValue1.Text = string.Empty;
                    tbValue2.Text = string.Empty;
                    optionalPassthru = true;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
            else
                DialogResult = DialogResult.OK;
        }
    }
}
