﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.IO;

using LimitsDatabase;

namespace TestEditor
{
    public partial class TestExporter : Form
    {
        private LimitsData limitsDB;
        //private LimitsData limitsDB = new LimitsData(Properties.Settings.Default.ExportServerIP, Properties.Settings.Default.ExportDatabase, Properties.Settings.Default.ExportUser,
        //Properties.Settings.Default.ExportPassword, "");
        string ip = string.Empty;
        string user = string.Empty;
        string password = string.Empty;
        string database = string.Empty;
        string availdatabase = string.Empty;

        public TestExporter()
        {
            InitializeComponent();
            //lExportingFrom.Text = Properties.Settings.Default.ExportServerIP + ":" + Properties.Settings.Default.ExportDatabase;
        }

        //------------ Utilitys
        private static int ExecuteCommand(string commnd, int timeout, ref string outputString, ref string errorString)
        {

            var pp = new ProcessStartInfo("cmd.exe", "/C" + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C:\\",
            };
            pp.RedirectStandardError = true;
            pp.RedirectStandardOutput = true;
            var process = Process.Start(pp);
            try
            {
                process.WaitForExit(timeout);
            }
            catch (Exception)
            {
                return -1;
            }
            using (StreamReader rdr = process.StandardOutput)
            {
                outputString = rdr.ReadToEnd();
            }
            using (StreamReader rdr = process.StandardError)
            {
                errorString = rdr.ReadToEnd();
            }
            process.Close();
            return 0;
        }


        //------------- Keys
        private void bGetCurrentLimits_Click(object sender, EventArgs e)
        {

            List<string> productList = new List<string>();
            string outmsg = string.Empty;
            ListViewItem lvpc = new ListViewItem();
            string pcFilter = string.Empty;
            string functionFilter = string.Empty;
            string filter = string.Empty;

            // get a list of existing limits
            limitsDB.GetListOfProductCodes(ref productList, ref outmsg);
            lvAllProducts.Items.Clear();
            GetTestListFilter iform = new GetTestListFilter(string.Empty, string.Empty);
            iform.ShowDialog();
            pcFilter = iform.PCFilter;
            functionFilter = iform.FunctionFilter;
            iform.Dispose();

            // data has the format <product code>#<version>#<function code>#<modify date>
            if ((pcFilter != string.Empty) | (functionFilter != string.Empty))
            {
                if (pcFilter != string.Empty)
                {
                    filter = pcFilter;
                }
                if (functionFilter != string.Empty)
                {
                    if (filter == string.Empty)
                    {
                        filter = ".*#.*#" + functionFilter;
                    }
                    else
                    {
                        filter = filter + "#.*#" + functionFilter;
                    }
                }
            }

            foreach (var item in productList)
            {
                if (filter != string.Empty)
                {
                    Match match = Regex.Match(item.Substring(0, item.IndexOf(':')), filter);
                    if (match.Success)
                    {
                        string[] parts = item.Split('#');
                        lvpc = new ListViewItem(parts[0]);
                        lvpc.SubItems.Add(parts[1]);
                        lvpc.SubItems.Add(parts[2]);
                        lvAllProducts.Items.Add(lvpc);
                    }
                }
                else
                {
                    string[] parts = item.Split('#');
                    lvpc = new ListViewItem(parts[0]);
                    lvpc.SubItems.Add(parts[1]);
                    lvpc.SubItems.Add(parts[2]);
                    lvAllProducts.Items.Add(lvpc);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cmd = string.Empty;
            string outputString = string.Empty;
            string errorString = string.Empty;
            //string wherepart = string.Empty;
            string databasepart = string.Empty;
            string outfilenamepart = string.Empty;

            LimitsData.product_codesItem alldata = new LimitsData.product_codesItem();
            Color oldcolor;

            if (lvAllProducts.SelectedItems.Count == 0)
            {
                MessageBox.Show("Must select a limit to export.");
                return;
            }

            oldcolor = bExport.BackColor;
            bExport.BackColor = Color.Green;

            for (int i = 0; i < lvAllProducts.SelectedItems.Count; i++)
            {
                errorString = string.Empty;
                bExport.Text = "WORKING:" + (i + 1);
                this.Refresh();
                limitsDB.GetData(lvAllProducts.SelectedItems[i].SubItems[0].Text, lvAllProducts.SelectedItems[i].SubItems[1].Text, lvAllProducts.SelectedItems[i].SubItems[2].Text, ref alldata, ref errorString);

                // make the  parts of the statments
                //wherepart = "product_code = '" + lvAllProducts.SelectedItems[i].SubItems[0].Text + "' and version = '" + lvAllProducts.SelectedItems[i].SubItems[1].Text +
                //                "' and function_code = '" + lvAllProducts.SelectedItems[i].SubItems[2].Text + "'";
                databasepart = "-h" + ip + " -u" + user + " -p" + password + " " + database;
                outfilenamepart = "\"" + Properties.Settings.Default.TransferDir + "\\" + lvAllProducts.SelectedItems[i].SubItems[0].Text +
                            alldata.function_code + lvAllProducts.SelectedItems[i].SubItems[1].Text + ".sql\"";

                // export product_codes table
                cmd = "mysqldump -t " + databasepart + " --tables product_codes --where=\"id = '" + alldata.guid + "'\" > " + outfilenamepart;
                if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
                {
                    if (!errorString.Contains("Warning"))   // ignore the warning
                        {
                        MessageBox.Show("Error making product codes. " + errorString);
                        bExport.BackColor = oldcolor;
                        bExport.Text = "Export";
                        return;
                    }
                }
                // export globals table
                cmd = "mysqldump -t " + databasepart + " --tables globals --where=\"product_codes_id = '" + alldata.guid + "'\" >> " + outfilenamepart;
                if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
                {
                    if (!errorString.Contains("Warning"))   // ignore the warning
                    {
                        MessageBox.Show("Error making globals. " + errorString);
                        bExport.BackColor = oldcolor;
                        bExport.Text = "Export";
                        return;
                    }
                }

                // export steps table
                cmd = "mysqldump -t " + databasepart + " --tables steps --where=\"product_codes_id = '" + alldata.guid + "'\" >> " + outfilenamepart;
                if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
                {
                    if (!errorString.Contains("Warning"))   // ignore the warning
                    {
                        MessageBox.Show("Error making steps. " + errorString);
                        bExport.BackColor = oldcolor;
                        bExport.Text = "Export";
                        return;
                    }
                }

                // loop through the steps
                foreach (var item in alldata.steps)
                {
                    // export parameters table
                    cmd = "mysqldump -t -h" + ip + " -u" + user + " -p" + password + " " + database + " --tables parameters --where=\"steps_id = '" + item.guid + "'\" >> " + outfilenamepart;
                    if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
                    {
                        if (!errorString.Contains("Warning"))   // ignore the warning
                        {
                            MessageBox.Show("Error making parameters. " + errorString);
                            bExport.BackColor = oldcolor;
                            bExport.Text = "Export";
                            return;
                        }
                    }

                    // export limits table
                    cmd = "mysqldump -t -h" + ip + " -u" + user + " -p" + password + " " + database + " --tables limits --where=\"steps_id = '" + item.guid + "'\" >> " + outfilenamepart;
                    if (ExecuteCommand(cmd, 10000, ref outputString, ref errorString) != 0)
                    {
                        if (!errorString.Contains("Warning"))   // ignore the warning
                        {
                            MessageBox.Show("Error making limits. " + errorString);
                            bExport.BackColor = oldcolor;
                            bExport.Text = "Export";
                            return;
                        }
                    }
                }
            }

            bExport.BackColor = oldcolor;
            bExport.Text = "Export";
        }

        private void TestExporter_Load(object sender, EventArgs e)
        {
            SelectServer iform = new SelectServer();
            iform.ShowDialog();
            serverDataItem selected = iform.selectedServerData;
            lExportingFrom.Text = iform.SelectedServer;
            lbTransferDir.Text = Properties.Settings.Default.TransferDir;
            limitsDB = new LimitsData(selected.ip, selected.database, selected.user, selected.password, selected.availTestDB);
            ip = selected.ip;
            user = selected.user;
            password = selected.password;
            database = selected.database;
        }
    }
}
