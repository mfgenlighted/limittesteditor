﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEditor
{
    public partial class GetProductCode : Form
    {
        public string ProductCode;
        public string Version;
        public string FunctionCode;

        public GetProductCode()
        {
            InitializeComponent();
            tbStartingVersionNumber.Text = "1.00";
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            ProductCode = tbProductCode.Text;
            Version = tbStartingVersionNumber.Text;
            FunctionCode = tbFuctionCode.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
