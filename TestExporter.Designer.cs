﻿namespace TestEditor
{
    partial class TestExporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bGetCurrentLimits = new System.Windows.Forms.Button();
            this.lvAllProducts = new System.Windows.Forms.ListView();
            this.lvProductCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvTestFunction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvModifyDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.bExport = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lExportingFrom = new System.Windows.Forms.Label();
            this.lbTransferDir = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bGetCurrentLimits
            // 
            this.bGetCurrentLimits.Location = new System.Drawing.Point(564, 193);
            this.bGetCurrentLimits.Name = "bGetCurrentLimits";
            this.bGetCurrentLimits.Size = new System.Drawing.Size(131, 23);
            this.bGetCurrentLimits.TabIndex = 10;
            this.bGetCurrentLimits.Text = "Get current limits";
            this.bGetCurrentLimits.UseVisualStyleBackColor = true;
            this.bGetCurrentLimits.Click += new System.EventHandler(this.bGetCurrentLimits_Click);
            // 
            // lvAllProducts
            // 
            this.lvAllProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvProductCode,
            this.lvVersion,
            this.lvTestFunction,
            this.lvModifyDate});
            this.lvAllProducts.FullRowSelect = true;
            this.lvAllProducts.GridLines = true;
            this.lvAllProducts.Location = new System.Drawing.Point(23, 42);
            this.lvAllProducts.Name = "lvAllProducts";
            this.lvAllProducts.Size = new System.Drawing.Size(427, 372);
            this.lvAllProducts.TabIndex = 9;
            this.lvAllProducts.UseCompatibleStateImageBehavior = false;
            this.lvAllProducts.View = System.Windows.Forms.View.Details;
            // 
            // lvProductCode
            // 
            this.lvProductCode.Text = "Product Code";
            this.lvProductCode.Width = 86;
            // 
            // lvVersion
            // 
            this.lvVersion.Text = "Version";
            this.lvVersion.Width = 67;
            // 
            // lvTestFunction
            // 
            this.lvTestFunction.Text = "Test Function";
            this.lvTestFunction.Width = 110;
            // 
            // lvModifyDate
            // 
            this.lvModifyDate.Text = "Modify Date";
            this.lvModifyDate.Width = 154;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Current products";
            // 
            // bExport
            // 
            this.bExport.Location = new System.Drawing.Point(580, 105);
            this.bExport.Name = "bExport";
            this.bExport.Size = new System.Drawing.Size(103, 23);
            this.bExport.TabIndex = 11;
            this.bExport.Text = "Export";
            this.bExport.UseVisualStyleBackColor = true;
            this.bExport.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(580, 147);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Done";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 421);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Exporting From";
            // 
            // lExportingFrom
            // 
            this.lExportingFrom.AutoSize = true;
            this.lExportingFrom.Location = new System.Drawing.Point(31, 441);
            this.lExportingFrom.Name = "lExportingFrom";
            this.lExportingFrom.Size = new System.Drawing.Size(19, 13);
            this.lExportingFrom.TabIndex = 14;
            this.lExportingFrom.Text = "----";
            // 
            // lbTransferDir
            // 
            this.lbTransferDir.AutoSize = true;
            this.lbTransferDir.Location = new System.Drawing.Point(164, 440);
            this.lbTransferDir.Name = "lbTransferDir";
            this.lbTransferDir.Size = new System.Drawing.Size(16, 13);
            this.lbTransferDir.TabIndex = 15;
            this.lbTransferDir.Text = "---";
            // 
            // TestExporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 466);
            this.Controls.Add(this.lbTransferDir);
            this.Controls.Add(this.lExportingFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bExport);
            this.Controls.Add(this.bGetCurrentLimits);
            this.Controls.Add(this.lvAllProducts);
            this.Controls.Add(this.label1);
            this.Name = "TestExporter";
            this.Text = "TestExporter";
            this.Load += new System.EventHandler(this.TestExporter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bGetCurrentLimits;
        private System.Windows.Forms.ListView lvAllProducts;
        private System.Windows.Forms.ColumnHeader lvProductCode;
        private System.Windows.Forms.ColumnHeader lvVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bExport;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lExportingFrom;
        private System.Windows.Forms.ColumnHeader lvTestFunction;
        private System.Windows.Forms.Label lbTransferDir;
        private System.Windows.Forms.ColumnHeader lvModifyDate;
    }
}