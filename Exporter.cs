﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace TestEditor
{
    class Exporter
    {

        public static int ExecuteCommand(string commnd, int timeout, ref string outputString, ref string errorString)
        {

            var pp = new ProcessStartInfo("cmd.exe", "/C" + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C:\\",
            };
            pp.RedirectStandardError = true;
            pp.RedirectStandardOutput = true;
            var process = Process.Start(pp);
            try
            {
                process.WaitForExit(timeout);
            }
            catch (Exception)
            {
                return -1;
            }
            using (StreamReader rdr = process.StandardOutput)
            {
                outputString = rdr.ReadToEnd();
            }
            using (StreamReader rdr = process.StandardError)
            {
                errorString = rdr.ReadToEnd();
            }
                process.Close();
            return 0;
        }

    }
}
