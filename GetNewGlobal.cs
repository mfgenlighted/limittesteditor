﻿using System;
using System.Windows.Forms;

namespace TestEditor
{
    public partial class GetNewGlobal : Form
    {
        public string GlobalName;
        public string GlobalValue;
        public GetNewGlobal()
        {
            InitializeComponent();
        }

        public GetNewGlobal(string name, string value)
        {
            InitializeComponent();
            tbName.Text = name;
            tbValue.Text = value;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            GlobalName = tbName.Text;
            GlobalValue = tbValue.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
