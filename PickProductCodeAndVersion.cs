﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class PickProductCodeAndVersion : Form
    {
        public string PickedProductCode = string.Empty;
        public string PickedVersion = string.Empty;
        public PickProductCodeAndVersion(LimitsData limitsDB)
        {
            ListViewItem lvitem = new ListViewItem();
            InitializeComponent();
            List<string> productList = new List<string>();
            string outmsg = string.Empty;
            string selectedProductCodeAndVersion = string.Empty;

            // get a list of existing limits
            limitsDB.GetListOfProductCodes(ref productList, ref outmsg);

            foreach (var item in productList)
            {
                string[] parts = item.Split('#');
                lvitem = new ListViewItem(parts[0]);
                lvitem.SubItems.Add(parts[1]);
                lvitem.SubItems.Add(parts[2]);
                lvAllProducts.Items.Add(lvitem);
            }

        }

        private void lvAllProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            PickedProductCode = lvAllProducts.SelectedItems[0].Text;
            PickedVersion = lvAllProducts.SelectedItems[0].SubItems[1].Text;
            DialogResult = DialogResult.OK;
        }
    }
}
