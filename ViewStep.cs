﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class ViewStep : Form
    {
        public LimitsData.stepsItem BuiltStep = new LimitsData.stepsItem();

        public ViewStep(LimitsData.stepsItem step)
        {
            InitializeComponent();

            BuiltStep = (LimitsData.stepsItem)step.Clone();


            // fill in spaces on form
            cbTestType.Text = step.test_type;
            cbActionOnFail.Text = step.do_on_fail;
            cbActionOnPass.Text = step.do_on_pass;
            nudMaxFails.Value = step.maxretries;
            nudPassAfterxPasses.Value = step.pass_on_x_tries;
            cbStepOperation.Text = step.step_operation;
            tbStepName.Text = step.name;
            if (step.limits != null)
            {
                if (step.limits.Count != 0)
                {
                    foreach (var item in step.limits)
                    {
                        ListViewItem onelvLimit = new ListViewItem(item.name);
                        onelvLimit.SubItems.Add(item.value1);
                        onelvLimit.SubItems.Add(item.value2);
                        onelvLimit.SubItems.Add(item.operation);
                        lvLimits.Items.Add(onelvLimit);
                    }
                }
            }
            if (step.parameters != null)
            {
                if (step.parameters.Count != 0)
                {
                    foreach (var item in step.parameters)
                    {
                        ListViewItem onelvParameter = new ListViewItem(item.name);
                        onelvParameter.SubItems.Add(item.value);
                        lvParameter.Items.Add(onelvParameter);
                    }
                }
            }

        }
    }
}
