﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TestEditor
{
    public partial class SelectServer : Form
    {
        public string SelectedServer = string.Empty;
        public serverDataItem selectedServerData = new serverDataItem();


        private List<serverDataItem> allserverData = new List<serverDataItem>();

        public SelectServer()
        {
            InitializeComponent();
        }

        //------------------------------
        // button events

        private void SelectServer_Load(object sender, EventArgs e)
        {
            bServer1.Text = Properties.Settings.Default.S1Name;
            bServer2.Text = Properties.Settings.Default.S2Name;
            bServer3.Text = Properties.Settings.Default.S3Name;
            bServer4.Text = Properties.Settings.Default.S4Name;

            serverDataItem item = new serverDataItem();
            item.name = Properties.Settings.Default.S1Name;
            item.ip = Properties.Settings.Default.S1IP;
            item.user = Properties.Settings.Default.S1User;
            item.password = Properties.Settings.Default.S1Password;
            item.database = Properties.Settings.Default.LimitsDatabase;
            allserverData.Add(item);

            item.name = Properties.Settings.Default.S2Name;
            item.ip = Properties.Settings.Default.S2IP;
            item.user = Properties.Settings.Default.S2User;
            item.password = Properties.Settings.Default.S2Password;
            item.database = Properties.Settings.Default.LimitsDatabase;
            allserverData.Add(item);

            item.name = Properties.Settings.Default.S3Name;
            item.ip = Properties.Settings.Default.S3IP;
            item.user = Properties.Settings.Default.S3User;
            item.password = Properties.Settings.Default.S3Password;
            item.database = Properties.Settings.Default.LimitsDatabase;
            allserverData.Add(item);

            item.name = Properties.Settings.Default.S4Name;
            item.ip = Properties.Settings.Default.S4IP;
            item.user = Properties.Settings.Default.S4User;
            item.password = Properties.Settings.Default.S4Password;
            item.database = Properties.Settings.Default.LimitsDatabase;
            allserverData.Add(item);

            rbCU.Text = Properties.Settings.Default.CUAvailDatabase;
            rbPacketbased.Text = Properties.Settings.Default.PacketSensorAvailDatabase;
            rbTextBase.Text = Properties.Settings.Default.TextSensorAvailDatabase;
            rbGW.Text = Properties.Settings.Default.GWAvailDatabase;
        }

        private void bServer1_Click(object sender, EventArgs e)
        {
            serverDataItem item = new serverDataItem();
            string msg = string.Empty;

            Button clickedbutton = (Button)sender;
            SelectedServer = clickedbutton.Text;

            item = allserverData.Find(x => x.name == clickedbutton.Text);
            selectedServerData = item;

            if (rbPacketbased.Checked)
                selectedServerData.availTestDB = rbPacketbased.Text;
            if (rbTextBase.Checked)
                selectedServerData.availTestDB = rbTextBase.Text;
            if (rbCU.Checked)
                selectedServerData.availTestDB = rbCU.Text;
            if (rbGW.Checked)
                selectedServerData.availTestDB = rbGW.Text;

            DialogResult = DialogResult.OK;

        }
    }
}
