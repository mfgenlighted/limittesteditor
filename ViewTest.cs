﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class ViewSteps : Form
    {
        LimitsData limitsDB;
        string productCode = string.Empty;
        float version = 1;
        string functionCode = string.Empty;
        LimitsData.product_codesItem product_code = new LimitsData.product_codesItem();

        public ViewSteps(LimitsData limitsDB, string code)
        {
            string[] parts;
            string outmsg = string.Empty;

            InitializeComponent();
            this.limitsDB = limitsDB;
            parts = code.Split(':');
            productCode = parts[0];
            version = float.Parse(parts[1]);
            functionCode = parts[2];
            this.lVersion.Text = version.ToString("N2");
            this.lProductCode.Text = productCode;

            limitsDB.GetData(productCode, version.ToString("N2"), functionCode, ref product_code, ref outmsg);
            // update the product code and version to the new ones
            if (product_code.steps != null)
            {
                foreach (var item in product_code.steps)
                {
                    ListViewItem lvsteps = new ListViewItem(item.step_number.ToString());
                    lvsteps.SubItems.Add(item.name.ToString());
                    lvsteps.SubItems.Add(item.function_call.ToString());
                    if (lvStepData == null)
                        lvStepData = new ListView();
                    lvStepData.Items.Add(lvsteps);
                }
            }
            if (product_code.globals != null)
            {
                foreach (var item in product_code.globals)
                {
                    ListViewItem lvglobal = new ListViewItem(item.name);
                    lvglobal.SubItems.Add(item.value);
                    if (lvGlobals == null)
                        lvGlobals = new ListView();
                    lvGlobals.Items.Add(lvglobal);
                }
            }

        }

        private void bView_Click(object sender, EventArgs e)
        {
            LimitsData.stepsItem step = product_code.steps[lvStepData.SelectedIndices[0]];
            ViewStep iform = new ViewStep(step);
            if (iform.ShowDialog() == DialogResult.OK)
            {
                int selectedIndex = lvStepData.SelectedIndices[0];
                // update the master stucture with the updated test
                try
                {
                    if (product_code.steps == null)
                        product_code.steps = new List<LimitsData.stepsItem>();
                    if (selectedIndex == 0)
                    {
                        product_code.steps.Add(iform.BuiltStep);
                        product_code.steps.RemoveAt(0);
                    }
                    else
                    {
                        product_code.steps.RemoveAt(selectedIndex);
                        product_code.steps.Insert(selectedIndex, iform.BuiltStep);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("saving step error: " + ex.Message);
                }
                iform.Dispose();
            }

        }
    }
}
