﻿namespace TestEditor
{
    partial class GetProductCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbProductCode = new System.Windows.Forms.TextBox();
            this.bOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbStartingVersionNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFuctionCode = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbProductCode
            // 
            this.tbProductCode.Location = new System.Drawing.Point(24, 37);
            this.tbProductCode.Name = "tbProductCode";
            this.tbProductCode.Size = new System.Drawing.Size(100, 20);
            this.tbProductCode.TabIndex = 0;
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(158, 145);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(75, 23);
            this.bOK.TabIndex = 1;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter new product code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Starting Version Numbe";
            // 
            // tbStartingVersionNumber
            // 
            this.tbStartingVersionNumber.Location = new System.Drawing.Point(24, 80);
            this.tbStartingVersionNumber.Name = "tbStartingVersionNumber";
            this.tbStartingVersionNumber.Size = new System.Drawing.Size(100, 20);
            this.tbStartingVersionNumber.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Enter Test Function Code";
            // 
            // tbFuctionCode
            // 
            this.tbFuctionCode.Location = new System.Drawing.Point(24, 130);
            this.tbFuctionCode.Name = "tbFuctionCode";
            this.tbFuctionCode.Size = new System.Drawing.Size(100, 20);
            this.tbFuctionCode.TabIndex = 8;
            // 
            // GetProductCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 201);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbFuctionCode);
            this.Controls.Add(this.tbStartingVersionNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.tbProductCode);
            this.Name = "GetProductCode";
            this.Text = "Get Product Code";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbProductCode;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbStartingVersionNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFuctionCode;
    }
}