﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEditor
{
    public partial class Get_ProductCodeAndVersion : Form
    {
        public string ProductVersionFunction = string.Empty;

        public Get_ProductCodeAndVersion()
        {
            InitializeComponent();
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            DialogResult =  DialogResult.OK;
            ProductVersionFunction = this.tbProductCode.Text + ":" + tbVersion.Text + ":" + tbFuctionCode.Text;
        }
    }
}
