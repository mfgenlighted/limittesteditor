﻿namespace TestEditor
{
    partial class PickProductCodeAndVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvAllProducts = new System.Windows.Forms.ListView();
            this.lvProductCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.lvFunctionCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvAllProducts
            // 
            this.lvAllProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvProductCode,
            this.lvVersion,
            this.lvFunctionCode});
            this.lvAllProducts.FullRowSelect = true;
            this.lvAllProducts.GridLines = true;
            this.lvAllProducts.Location = new System.Drawing.Point(61, 61);
            this.lvAllProducts.Name = "lvAllProducts";
            this.lvAllProducts.Size = new System.Drawing.Size(243, 204);
            this.lvAllProducts.TabIndex = 5;
            this.lvAllProducts.UseCompatibleStateImageBehavior = false;
            this.lvAllProducts.View = System.Windows.Forms.View.Details;
            this.lvAllProducts.SelectedIndexChanged += new System.EventHandler(this.lvAllProducts_SelectedIndexChanged);
            // 
            // lvProductCode
            // 
            this.lvProductCode.Text = "Product Code";
            this.lvProductCode.Width = 86;
            // 
            // lvVersion
            // 
            this.lvVersion.Text = "Version";
            this.lvVersion.Width = 58;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Click on the test you want to base the new test on";
            // 
            // lvFunctionCode
            // 
            this.lvFunctionCode.Text = "Function Code";
            this.lvFunctionCode.Width = 82;
            // 
            // PickProductCodeAndVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 411);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvAllProducts);
            this.Name = "PickProductCodeAndVersion";
            this.Text = "PickProductCodeAndVersion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvAllProducts;
        private System.Windows.Forms.ColumnHeader lvProductCode;
        private System.Windows.Forms.ColumnHeader lvVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader lvFunctionCode;
    }
}