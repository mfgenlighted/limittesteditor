﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEditor
{
    public partial class GetComment : Form
    {
        public string Comment;
        public GetComment()
        {
            InitializeComponent();
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            Comment = tbComment.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
