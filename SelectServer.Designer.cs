﻿namespace TestEditor
{
    partial class SelectServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bServer4 = new System.Windows.Forms.Button();
            this.bServer2 = new System.Windows.Forms.Button();
            this.bServer3 = new System.Windows.Forms.Button();
            this.bServer1 = new System.Windows.Forms.Button();
            this.rbPacketbased = new System.Windows.Forms.RadioButton();
            this.rbTextBase = new System.Windows.Forms.RadioButton();
            this.rbCU = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbGW = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bServer4
            // 
            this.bServer4.Location = new System.Drawing.Point(23, 123);
            this.bServer4.Name = "bServer4";
            this.bServer4.Size = new System.Drawing.Size(210, 23);
            this.bServer4.TabIndex = 7;
            this.bServer4.Text = "button4";
            this.bServer4.UseVisualStyleBackColor = true;
            this.bServer4.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // bServer2
            // 
            this.bServer2.Location = new System.Drawing.Point(23, 65);
            this.bServer2.Name = "bServer2";
            this.bServer2.Size = new System.Drawing.Size(210, 23);
            this.bServer2.TabIndex = 6;
            this.bServer2.Text = "button3";
            this.bServer2.UseVisualStyleBackColor = true;
            this.bServer2.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // bServer3
            // 
            this.bServer3.Location = new System.Drawing.Point(23, 94);
            this.bServer3.Name = "bServer3";
            this.bServer3.Size = new System.Drawing.Size(210, 23);
            this.bServer3.TabIndex = 5;
            this.bServer3.Text = "button2";
            this.bServer3.UseVisualStyleBackColor = true;
            this.bServer3.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // bServer1
            // 
            this.bServer1.Location = new System.Drawing.Point(23, 33);
            this.bServer1.Name = "bServer1";
            this.bServer1.Size = new System.Drawing.Size(210, 23);
            this.bServer1.TabIndex = 4;
            this.bServer1.Text = "button1";
            this.bServer1.UseVisualStyleBackColor = true;
            this.bServer1.Click += new System.EventHandler(this.bServer1_Click);
            // 
            // rbPacketbased
            // 
            this.rbPacketbased.AutoSize = true;
            this.rbPacketbased.Checked = true;
            this.rbPacketbased.Location = new System.Drawing.Point(6, 19);
            this.rbPacketbased.Name = "rbPacketbased";
            this.rbPacketbased.Size = new System.Drawing.Size(85, 17);
            this.rbPacketbased.TabIndex = 8;
            this.rbPacketbased.TabStop = true;
            this.rbPacketbased.Text = "radioButton1";
            this.rbPacketbased.UseVisualStyleBackColor = true;
            // 
            // rbTextBase
            // 
            this.rbTextBase.AutoSize = true;
            this.rbTextBase.Location = new System.Drawing.Point(6, 42);
            this.rbTextBase.Name = "rbTextBase";
            this.rbTextBase.Size = new System.Drawing.Size(85, 17);
            this.rbTextBase.TabIndex = 9;
            this.rbTextBase.TabStop = true;
            this.rbTextBase.Text = "radioButton2";
            this.rbTextBase.UseVisualStyleBackColor = true;
            // 
            // rbCU
            // 
            this.rbCU.AutoSize = true;
            this.rbCU.Location = new System.Drawing.Point(6, 65);
            this.rbCU.Name = "rbCU";
            this.rbCU.Size = new System.Drawing.Size(85, 17);
            this.rbCU.TabIndex = 10;
            this.rbCU.TabStop = true;
            this.rbCU.Text = "radioButton3";
            this.rbCU.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbGW);
            this.groupBox1.Controls.Add(this.rbCU);
            this.groupBox1.Controls.Add(this.rbTextBase);
            this.groupBox1.Controls.Add(this.rbPacketbased);
            this.groupBox1.Location = new System.Drawing.Point(292, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 146);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // rbGW
            // 
            this.rbGW.AutoSize = true;
            this.rbGW.Location = new System.Drawing.Point(7, 95);
            this.rbGW.Name = "rbGW";
            this.rbGW.Size = new System.Drawing.Size(53, 17);
            this.rbGW.TabIndex = 11;
            this.rbGW.TabStop = true;
            this.rbGW.Text = "rbGW";
            this.rbGW.UseVisualStyleBackColor = true;
            // 
            // SelectServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 273);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bServer4);
            this.Controls.Add(this.bServer2);
            this.Controls.Add(this.bServer3);
            this.Controls.Add(this.bServer1);
            this.Name = "SelectServer";
            this.Text = "SelectServer";
            this.Load += new System.EventHandler(this.SelectServer_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bServer4;
        private System.Windows.Forms.Button bServer2;
        private System.Windows.Forms.Button bServer3;
        private System.Windows.Forms.Button bServer1;
        private System.Windows.Forms.RadioButton rbPacketbased;
        private System.Windows.Forms.RadioButton rbTextBase;
        private System.Windows.Forms.RadioButton rbCU;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbGW;
    }
}