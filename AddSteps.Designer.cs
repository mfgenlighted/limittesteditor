﻿namespace TestEditor
{
    partial class AddSteps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lVersion = new System.Windows.Forms.Label();
            this.lProductCode = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lvStepData = new System.Windows.Forms.ListView();
            this.cStepNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cStepName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbTestList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.bMoveUp = new System.Windows.Forms.Button();
            this.bMoveDown = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bEdit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lvGlobals = new System.Windows.Forms.ListView();
            this.global_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.global_value = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bEditGlobal = new System.Windows.Forms.Button();
            this.bDeleteGlobal = new System.Windows.Forms.Button();
            this.bAddGlobal = new System.Windows.Forms.Button();
            this.lFunction = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cFunctionCall = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Version";
            // 
            // lVersion
            // 
            this.lVersion.AutoSize = true;
            this.lVersion.Location = new System.Drawing.Point(89, 32);
            this.lVersion.Name = "lVersion";
            this.lVersion.Size = new System.Drawing.Size(19, 13);
            this.lVersion.TabIndex = 3;
            this.lVersion.Text = "----";
            // 
            // lProductCode
            // 
            this.lProductCode.AutoSize = true;
            this.lProductCode.Location = new System.Drawing.Point(12, 32);
            this.lProductCode.Name = "lProductCode";
            this.lProductCode.Size = new System.Drawing.Size(19, 13);
            this.lProductCode.TabIndex = 2;
            this.lProductCode.Text = "----";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Steps";
            // 
            // lvStepData
            // 
            this.lvStepData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cStepNumber,
            this.cStepName,
            this.cFunctionCall});
            this.lvStepData.FullRowSelect = true;
            this.lvStepData.GridLines = true;
            this.lvStepData.Location = new System.Drawing.Point(15, 79);
            this.lvStepData.Name = "lvStepData";
            this.lvStepData.Size = new System.Drawing.Size(503, 465);
            this.lvStepData.TabIndex = 6;
            this.lvStepData.UseCompatibleStateImageBehavior = false;
            this.lvStepData.View = System.Windows.Forms.View.Details;
            this.lvStepData.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbTestList_Click);
            // 
            // cStepNumber
            // 
            this.cStepNumber.Text = "Step Number";
            this.cStepNumber.Width = 82;
            // 
            // cStepName
            // 
            this.cStepName.Text = "Step Name";
            this.cStepName.Width = 195;
            // 
            // cbTestList
            // 
            this.cbTestList.FormattingEnabled = true;
            this.cbTestList.Location = new System.Drawing.Point(567, 208);
            this.cbTestList.Name = "cbTestList";
            this.cbTestList.Size = new System.Drawing.Size(155, 21);
            this.cbTestList.TabIndex = 7;
            this.cbTestList.SelectedIndexChanged += new System.EventHandler(this.cbTestList_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(598, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Pick a test to add";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(888, 495);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 28);
            this.button1.TabIndex = 9;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(991, 495);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 28);
            this.bSave.TabIndex = 10;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bMoveUp
            // 
            this.bMoveUp.Location = new System.Drawing.Point(601, 77);
            this.bMoveUp.Name = "bMoveUp";
            this.bMoveUp.Size = new System.Drawing.Size(75, 23);
            this.bMoveUp.TabIndex = 11;
            this.bMoveUp.Text = "Move Up";
            this.bMoveUp.UseVisualStyleBackColor = true;
            this.bMoveUp.Click += new System.EventHandler(this.bMoveUp_Click);
            // 
            // bMoveDown
            // 
            this.bMoveDown.Location = new System.Drawing.Point(601, 106);
            this.bMoveDown.Name = "bMoveDown";
            this.bMoveDown.Size = new System.Drawing.Size(75, 23);
            this.bMoveDown.TabIndex = 12;
            this.bMoveDown.Text = "Move Down";
            this.bMoveDown.UseVisualStyleBackColor = true;
            this.bMoveDown.Click += new System.EventHandler(this.bMoveDown_Click);
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(601, 137);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 13;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bEdit
            // 
            this.bEdit.Location = new System.Drawing.Point(601, 166);
            this.bEdit.Name = "bEdit";
            this.bEdit.Size = new System.Drawing.Size(75, 23);
            this.bEdit.TabIndex = 14;
            this.bEdit.Text = "Edit";
            this.bEdit.UseVisualStyleBackColor = true;
            this.bEdit.Click += new System.EventHandler(this.bEdit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(777, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Globals";
            // 
            // lvGlobals
            // 
            this.lvGlobals.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.global_name,
            this.global_value});
            this.lvGlobals.FullRowSelect = true;
            this.lvGlobals.GridLines = true;
            this.lvGlobals.Location = new System.Drawing.Point(777, 99);
            this.lvGlobals.Name = "lvGlobals";
            this.lvGlobals.Size = new System.Drawing.Size(289, 206);
            this.lvGlobals.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvGlobals.TabIndex = 16;
            this.lvGlobals.UseCompatibleStateImageBehavior = false;
            this.lvGlobals.View = System.Windows.Forms.View.Details;
            // 
            // global_name
            // 
            this.global_name.Text = "Name";
            this.global_name.Width = 82;
            // 
            // global_value
            // 
            this.global_value.Text = "Value";
            this.global_value.Width = 195;
            // 
            // bEditGlobal
            // 
            this.bEditGlobal.Location = new System.Drawing.Point(873, 311);
            this.bEditGlobal.Name = "bEditGlobal";
            this.bEditGlobal.Size = new System.Drawing.Size(75, 23);
            this.bEditGlobal.TabIndex = 18;
            this.bEditGlobal.Text = "Edit";
            this.bEditGlobal.UseVisualStyleBackColor = true;
            this.bEditGlobal.Click += new System.EventHandler(this.bEditGlobal_Click);
            // 
            // bDeleteGlobal
            // 
            this.bDeleteGlobal.Location = new System.Drawing.Point(780, 311);
            this.bDeleteGlobal.Name = "bDeleteGlobal";
            this.bDeleteGlobal.Size = new System.Drawing.Size(75, 23);
            this.bDeleteGlobal.TabIndex = 17;
            this.bDeleteGlobal.Text = "Delete";
            this.bDeleteGlobal.UseVisualStyleBackColor = true;
            this.bDeleteGlobal.Click += new System.EventHandler(this.bDeleteGlobal_Click);
            // 
            // bAddGlobal
            // 
            this.bAddGlobal.Location = new System.Drawing.Point(963, 311);
            this.bAddGlobal.Name = "bAddGlobal";
            this.bAddGlobal.Size = new System.Drawing.Size(103, 23);
            this.bAddGlobal.TabIndex = 19;
            this.bAddGlobal.Text = "Add a Global";
            this.bAddGlobal.UseVisualStyleBackColor = true;
            this.bAddGlobal.Click += new System.EventHandler(this.bAddGlobal_Click);
            // 
            // lFunction
            // 
            this.lFunction.AutoSize = true;
            this.lFunction.Location = new System.Drawing.Point(156, 32);
            this.lFunction.Name = "lFunction";
            this.lFunction.Size = new System.Drawing.Size(19, 13);
            this.lFunction.TabIndex = 21;
            this.lFunction.Text = "----";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Function";
            // 
            // cFunctionCall
            // 
            this.cFunctionCall.Text = "Function Call";
            this.cFunctionCall.Width = 222;
            // 
            // AddSteps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 567);
            this.Controls.Add(this.lFunction);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.bAddGlobal);
            this.Controls.Add(this.bEditGlobal);
            this.Controls.Add(this.bDeleteGlobal);
            this.Controls.Add(this.lvGlobals);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bEdit);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bMoveDown);
            this.Controls.Add(this.bMoveUp);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbTestList);
            this.Controls.Add(this.lvStepData);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lVersion);
            this.Controls.Add(this.lProductCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddSteps";
            this.Text = "Add Steps";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lVersion;
        private System.Windows.Forms.Label lProductCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvStepData;
        private System.Windows.Forms.ColumnHeader cStepNumber;
        private System.Windows.Forms.ColumnHeader cStepName;
        private System.Windows.Forms.ComboBox cbTestList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bMoveUp;
        private System.Windows.Forms.Button bMoveDown;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bEdit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView lvGlobals;
        private System.Windows.Forms.ColumnHeader global_name;
        private System.Windows.Forms.ColumnHeader global_value;
        private System.Windows.Forms.Button bEditGlobal;
        private System.Windows.Forms.Button bDeleteGlobal;
        private System.Windows.Forms.Button bAddGlobal;
        private System.Windows.Forms.Label lFunction;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ColumnHeader cFunctionCall;
    }
}