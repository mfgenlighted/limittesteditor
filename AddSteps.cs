﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class AddSteps : Form
    {

        LimitsData limitsDB;
        string productCode = string.Empty;
        float version = 1;
        string functionCode = string.Empty;
        int stepNumber = 1;
        List<LimitsData.avai_testsItem> avaiTests = new List<LimitsData.avai_testsItem>();
        LimitsData.product_codesItem product_code = new LimitsData.product_codesItem();

        /// <summary>
        /// Make a new product code/version set of limits
        /// </summary>
        /// <param name="limitsDB"></param>
        /// <param name="code"></param>
        public AddSteps(LimitsData limitsDB, string code)
        {
            string outmsg = string.Empty;
            string[] parts = code.Split(':');

            InitializeComponent();
            this.limitsDB = limitsDB;

            productCode = parts[0];
            functionCode = parts[2];
            version = 1;
            this.lVersion.Text = "1.00";
            this.lProductCode.Text = productCode;
            this.lFunction.Text = parts[2];

            product_code.product_code = productCode;
            product_code.version = version;
            product_code.function_code = functionCode;

            limitsDB.GetListOfAvaiTests(ref avaiTests, ref outmsg);
            foreach (var item in avaiTests)
            {
                cbTestList.Items.Add(item.name);
            }

        }

        /// <summary>
        /// Make a new item based on existing item.
        /// </summary>
        /// <param name="limitsDB"></param>
        /// <param name="code">new productcode:version tags</param>
        /// <param name="basedOnCode">productcode:version to base new one on</param>
        public AddSteps(LimitsData limitsDB, string code, string basedOnCode)
        {
            string outmsg = string.Empty;
            string basedOnProductCode = string.Empty;
            string basedOnVersion = string.Empty;
            string basedOnFunction = string.Empty;
            List<string> codes = new List<string>();
            string[] parts;
            string[] basedOnParts;

            InitializeComponent();
            this.limitsDB = limitsDB;
            parts = code.Split(':');
            productCode = parts[0];
            version = float.Parse(parts[1]);
            functionCode = parts[2];
            this.lVersion.Text = version.ToString("N2");
            this.lProductCode.Text = productCode;
            //fill in steps based on passed code/version
            basedOnParts = basedOnCode.Split(':');
            basedOnProductCode = basedOnParts[0];
            basedOnVersion = basedOnParts[1];
            basedOnFunction = basedOnParts[2];


            limitsDB.GetListOfAvaiTests(ref avaiTests, ref outmsg);
            foreach (var item in avaiTests)
            {
                cbTestList.Items.Add(item.name);
            }

            limitsDB.GetData(basedOnProductCode, basedOnVersion, basedOnFunction, ref product_code, ref outmsg);
            // update the product code and version to the new ones
            if (product_code.steps != null)
            {
                foreach (var item in product_code.steps)
                {
                    ListViewItem lvsteps = new ListViewItem(item.step_number.ToString());
                    lvsteps.SubItems.Add(item.name.ToString());
                    lvsteps.SubItems.Add(item.function_call.ToString());
                    if (lvStepData == null)
                        lvStepData = new ListView();
                    lvStepData.Items.Add(lvsteps);
                }
            }
            if (product_code.globals != null)
            {
                foreach (var item in product_code.globals)
                {
                    ListViewItem lvglobal = new ListViewItem(item.name);
                    lvglobal.SubItems.Add(item.value);
                    if (lvGlobals == null)
                        lvGlobals = new ListView();
                    lvGlobals.Items.Add(lvglobal);
                }
            }
            stepNumber = product_code.steps.Count;
            product_code.product_code = productCode;
            product_code.version = version;
            product_code.function_code = functionCode;
       }

        /// <summary>
        /// this will add a step
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbTestList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LimitsData.stepsItem  steps = new LimitsData.stepsItem();
            ListViewItem lvi;

            // Get the step data
            StepSetupcs iform = new StepSetupcs(avaiTests[cbTestList.SelectedIndex]);
            if (iform.ShowDialog() == DialogResult.OK)
            {
                steps = iform.BuiltStep;
                iform.Dispose();
                steps.step_number = stepNumber++;                       // add to bottom of list
                lvi = new ListViewItem(steps.step_number.ToString());
                lvi.SubItems.Add(steps.name);
                lvi.SubItems.Add(steps.function_call);
                lvStepData.Items.Add(lvi);
                try
                {
                    if (product_code.steps == null)
                        product_code.steps = new List<LimitsData.stepsItem>();
                    product_code.steps.Add(steps);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("saving step error: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// edit a step
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="mouse"></param>
        private void cbTestList_Click(object sender, MouseEventArgs mouse)
        {
            LimitsData.stepsItem steps = new LimitsData.stepsItem();

            LimitsData.stepsItem step = product_code.steps[lvStepData.SelectedIndices[0]];
            StepSetupcs iform = new StepSetupcs(step);
            if (mouse.Button == MouseButtons.Right)
            {
                if (MessageBox.Show("Do you want to edit this step?", "Step Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (iform.ShowDialog() == DialogResult.OK)
                    {
                        int selectedIndex = lvStepData.SelectedIndices[0];
                        // update the master structure with the updated step
                        try
                        {
                            if (product_code.steps == null)
                                product_code.steps = new List<LimitsData.stepsItem>();
                            if (selectedIndex == 0)
                            {
                                product_code.steps.Add(iform.BuiltStep);
                                product_code.steps.RemoveAt(0);
                            }
                            else
                            {
                                product_code.steps.RemoveAt(selectedIndex);
                                product_code.steps.Insert(selectedIndex, iform.BuiltStep);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("saving step error: " + ex.Message);
                        }
                        iform.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Edit the selected step
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bEdit_Click(object sender, EventArgs e)
        {

            // Get the step data
            if (lvStepData.SelectedIndices.Count == 0)
                return;

            LimitsData.stepsItem step = product_code.steps[lvStepData.SelectedIndices[0]];
            StepSetupcs iform = new StepSetupcs(step);
            if (MessageBox.Show("Do you want to edit this step?", "Step Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (iform.ShowDialog() == DialogResult.OK)
                {
                    int selectedIndex = lvStepData.SelectedIndices[0];
                    // update the master stucture with the updated test
                    try
                    {
                        if (product_code.steps == null)
                            product_code.steps = new List<LimitsData.stepsItem>();
                        if (selectedIndex == 0)
                        {
                            product_code.steps.Add(iform.BuiltStep);
                            product_code.steps.RemoveAt(0);
                        }
                        else
                        {
                            product_code.steps.RemoveAt(selectedIndex);
                            product_code.steps.Insert(selectedIndex, iform.BuiltStep);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("saving step error: " + ex.Message);
                    }
                    iform.Dispose();
                }
                // redisply the display table
                lvStepData.Items.Clear();
                if (product_code.steps != null)
                {
                    foreach (var item in product_code.steps)
                    {
                        ListViewItem lvsteps = new ListViewItem(item.step_number.ToString());
                        lvsteps.SubItems.Add(item.name.ToString());
                        lvsteps.SubItems.Add(item.function_call.ToString());
                        if (lvStepData == null)
                            lvStepData = new ListView();
                        lvStepData.Items.Add(lvsteps);
                    }
                }
            }

        }


        private void bSave_Click(object sender, EventArgs e)
        {
            if (product_code.steps == null)          // if no steps defined
            {
                MessageBox.Show("No stesp are defined. Test will not be saveed.");
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                string outmsg = string.Empty;
                GetComment comment = new GetComment();
                comment.ShowDialog();
                limitsDB.SaveData(product_code, comment.Comment, ref outmsg);
                DialogResult = DialogResult.OK;
            }
        }

        /// <summary>
        /// delete a step. adjust all the step numbers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDelete_Click(object sender, EventArgs e)
        {
            LimitsData.stepsItem steps = new LimitsData.stepsItem();

            if (lvStepData.SelectedIndices.Count == 0)          // if nothing selected
                return;

            // Get the step data
            int selectedIndex = lvStepData.SelectedIndices[0];      // get the table location
            if (MessageBox.Show("Do you want to delete this step?", "Step Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // modfiy the product_code structure
                product_code.steps.RemoveAt(selectedIndex);             // get rid of the step
                for (int i = 0; i < product_code.steps.Count; i++)
                {
                    steps = product_code.steps[i];
                    steps.step_number = i + 1;
                    product_code.steps[i] = steps;
                }
                // redisply the display table
                lvStepData.Items.Clear();
                if (product_code.steps != null)
                {
                    foreach (var item in product_code.steps)
                    {
                        ListViewItem lvsteps = new ListViewItem(item.step_number.ToString());
                        lvsteps.SubItems.Add(item.name.ToString());
                        lvsteps.SubItems.Add(item.function_call.ToString());
                        if (lvStepData == null)
                            lvStepData = new ListView();
                        lvStepData.Items.Add(lvsteps);
                    }
                }

            }

        }


        /// <summary>
        /// Move a step up in the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bMoveUp_Click(object sender, EventArgs e)
        {
            int fromTestNumber = 0;
            int toTestNumber = 0;

            if (lvStepData.SelectedIndices.Count == 0)
                return;
            if (lvStepData.SelectedIndices[0] == 0)     // if at top already
                return;
            // need to swap the step number with the one above.
            fromTestNumber = lvStepData.SelectedIndices[0] + 1;
            toTestNumber = fromTestNumber - 1;

            LimitsData.stepsItem bss = new LimitsData.stepsItem();      // place to save data
            bss = (LimitsData.stepsItem)product_code.steps[lvStepData.SelectedIndices[0]].Clone();
            bss.step_number = bss.step_number - 1;
            product_code.steps.RemoveAt(lvStepData.SelectedIndices[0]);
            product_code.steps.Insert(lvStepData.SelectedIndices[0] - 1, bss);
            product_code.steps[lvStepData.SelectedIndices[0]].step_number = product_code.steps[lvStepData.SelectedIndices[0]].step_number + 1;

            lvStepData.Items.Clear();
            foreach (var item in product_code.steps)
            {
                ListViewItem lvsteps = new ListViewItem(item.step_number.ToString());
                lvsteps.SubItems.Add(item.name.ToString());
                lvsteps.SubItems.Add(item.function_call.ToString());
                if (lvStepData == null)
                    lvStepData = new ListView();
                lvStepData.Items.Add(lvsteps);
            }
        }

        private void bMoveDown_Click(object sender, EventArgs e)
        {
            int fromTestNumber = 0;
            int toTestNumber = 0;

            if (lvStepData.SelectedIndices.Count == 0)
                return;
            if (lvStepData.SelectedIndices[0] == (lvStepData.Items.Count - 1))     // if at botttom already
                return;
            // need to swap the step number with the one below.
            fromTestNumber = lvStepData.SelectedIndices[0] + 1;         // convert from index(zero based) to test number(1 based)
            toTestNumber = fromTestNumber + 1;

            LimitsData.stepsItem bss = new LimitsData.stepsItem();      // place to save data
            bss = (LimitsData.stepsItem)product_code.steps[lvStepData.SelectedIndices[0]].Clone();
            bss.step_number = bss.step_number + 1;
            product_code.steps.RemoveAt(lvStepData.SelectedIndices[0]);
            product_code.steps.Insert(lvStepData.SelectedIndices[0] + 1, bss);
            product_code.steps[lvStepData.SelectedIndices[0]].step_number = product_code.steps[lvStepData.SelectedIndices[0]].step_number - 1;

            lvStepData.Items.Clear();
            foreach (var item in product_code.steps)
            {
                ListViewItem lvsteps = new ListViewItem(item.step_number.ToString());
                lvsteps.SubItems.Add(item.name.ToString());
                lvsteps.SubItems.Add(item.function_call.ToString());
                if (lvStepData == null)
                    lvStepData = new ListView();
                lvStepData.Items.Add(lvsteps);
            }
        }

        private void bAddGlobal_Click(object sender, EventArgs e)
        {
            LimitsData.globalsItem globalItem = new LimitsData.globalsItem();
            ListViewItem lvi;

            // Get the global data
            GetNewGlobal iform = new GetNewGlobal();
            if (iform.ShowDialog() == DialogResult.OK)
            {
                lvi = new ListViewItem(iform.GlobalName);
                lvi.SubItems.Add(iform.GlobalValue);
                lvGlobals.Items.Add(lvi);
                globalItem.name = iform.GlobalName;
                globalItem.value = iform.GlobalValue;
                iform.Dispose();


                try
                {
                    if (product_code.globals  == null)
                        product_code.globals = new List<LimitsData.globalsItem>();
                    product_code.globals.Add(globalItem);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("saving global error: " + ex.Message);
                }
            }
        }
        /// <summary>
        /// Delete a global item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDeleteGlobal_Click(object sender, EventArgs e)
        {
            if (lvGlobals.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Delete global " + lvGlobals.SelectedItems[0].Text + "?", "Globa Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    lvGlobals.Items[lvGlobals.SelectedIndices[0]].Remove();
                    product_code.globals.Clear();           // rebuilt the table
                    foreach (ListViewItem item in lvGlobals.Items)
                    {
                        LimitsData.globalsItem lclglobal= new LimitsData.globalsItem();
                        lclglobal.name = item.Text;
                        lclglobal.value = item.SubItems[1].Text;
                        product_code.globals.Add(lclglobal);
                    }
                }
            }
        }

        /// <summary>
        /// Edit a global item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bEditGlobal_Click(object sender, EventArgs e)
        {
            GetNewGlobal iform = new GetNewGlobal(lvGlobals.SelectedItems[0].Text, lvGlobals.SelectedItems[0].SubItems[1].Text);
            if (iform.ShowDialog() == DialogResult.OK)
            {
                lvGlobals.SelectedItems[0].Text = iform.GlobalName;
                lvGlobals.SelectedItems[0].SubItems[1].Text = iform.GlobalValue;
            }
            product_code.globals.Clear();           // rebuilt the table
            foreach (ListViewItem item in lvGlobals.Items)
            {
                LimitsData.globalsItem lclglobal = new LimitsData.globalsItem();
                lclglobal.name = item.Text;
                lclglobal.value = item.SubItems[1].Text;
                product_code.globals.Add(lclglobal);
            }
        }

    }
}
