﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class StepSetupcs : Form
    {
        public LimitsData.stepsItem BuiltStep = new LimitsData.stepsItem();


        // bring up a step setup based on a database step
        public StepSetupcs(LimitsData.stepsItem step)
        {

            InitializeComponent();

            BuiltStep = (LimitsData.stepsItem)step.Clone();


            // fill in spaces on form
            cbTestType.Text = step.test_type ;
            cbActionOnFail.Text = step.do_on_fail;
            cbActionOnPass.Text = step.do_on_pass;
            nudMaxFails.Value = step.maxretries;
            nudPassAfterxPasses.Value = step.pass_on_x_tries;
            cbStepOperation.Text = step.step_operation;
            tbStepName.Text = step.name;
            if (step.limits != null)
            {
                if (step.limits.Count != 0)
                {
                    foreach (var item in step.limits)
                    {
                        ListViewItem onelvLimit= new ListViewItem(item.name);
                        onelvLimit.SubItems.Add(item.value1);
                        onelvLimit.SubItems.Add(item.value2);
                        onelvLimit.SubItems.Add(item.operation);
                        lvLimits.Items.Add(onelvLimit);
                    }
                }
            }
            if (step.parameters != null)
            {
                if (step.parameters.Count != 0)
                {
                    foreach (var item in step.parameters)
                    {
                        ListViewItem onelvParameter = new ListViewItem(item.name);
                        onelvParameter.SubItems.Add(item.value);
                        lvParameter.Items.Add(onelvParameter);
                    }
                }
            }
        }

        /// <summary>
        /// will start a new test based on the avai_testItem
        /// </summary>
        /// <param name="step"></param>
        public StepSetupcs(LimitsData.avai_testsItem step)
        {

            InitializeComponent();

            // make the new test
            BuiltStep = new LimitsData.stepsItem();         // start with blank test
            BuiltStep.name = step.name;
            BuiltStep.function_call = step.function;
            BuiltStep.test_type = step.testType;
            BuiltStep.do_on_fail = "END";
            BuiltStep.do_on_pass = "NEXT";
            BuiltStep.maxretries = 0;
            BuiltStep.pass_on_x_tries = 0;
            BuiltStep.step_operation = "RUN";
            if (step.limits != null)
            {
                if (step.limits.Count != 0)
                {
                    BuiltStep.limits = new List<LimitsData.limitsItem>();
                    foreach (var item in step.limits)
                    {
                        BuiltStep.limits.Add(item);
                    }
                }
            }
            if (step.parameters != null)
            {
                if (step.parameters.Count != 0)
                {
                    BuiltStep.parameters = new List<LimitsData.parametersItem>();
                    foreach (var item in step.parameters)
                    {
                        BuiltStep.parameters.Add(item);
                    }
                }
            }


            // fill in spaces on form
            cbTestType.Text = step.testType;
            cbActionOnFail.Text = "END";
            cbActionOnPass.Text = "NEXT";
            nudMaxFails.Value = 0;
            nudPassAfterxPasses.Value = 0;
            cbStepOperation.Text = "RUN";
            tbStepName.Text = step.name;
            if (step.limits != null)
            {
                if (step.limits.Count != 0)
                {
                    foreach (var item in step.limits)
                    {
                        ListViewItem onelvLimit = new ListViewItem(item.name);
                        onelvLimit.SubItems.Add(item.value1);
                        onelvLimit.SubItems.Add(item.value2);
                        onelvLimit.SubItems.Add(item.operation);
                        lvLimits.Items.Add(onelvLimit);
                    }
                }
            }
            if (step.parameters != null)
            {
                if (step.parameters.Count != 0)
                {
                    foreach (var item in step.parameters)
                    {
                        ListViewItem onelvParameter = new ListViewItem(item.name);
                        onelvParameter.SubItems.Add(item.value);
                        lvParameter.Items.Add(onelvParameter);
                    }
                }
            }
        }

        /// <summary>
        /// BuiltStep has the uppdated values. Just return
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bSave_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void bAddParameters_Click(object sender, EventArgs e)
        {
            ParameterSetup iform = new ParameterSetup();
            iform.ShowDialog();
            if (BuiltStep.parameters == null)
                BuiltStep.parameters = new List<LimitsData.parametersItem>();
            BuiltStep.parameters.AddRange(iform.ParamList);

            lvParameter.Items.Clear();
            foreach (var titem in BuiltStep.parameters)
            {
                ListViewItem oneparam = new ListViewItem(titem.name);
                oneparam.SubItems.Add(titem.value);
                lvParameter.Items.Add(oneparam);
            }
            iform.Dispose();
       }

        private void bAddLimits_Click(object sender, EventArgs e)
        {
            LimitsSetup iform = new LimitsSetup();
            iform.ShowDialog();
            if (BuiltStep.limits == null)
                BuiltStep.limits = new List<LimitsData.limitsItem>();
            BuiltStep.limits.AddRange(iform.LimitList);

            lvLimits.Items.Clear();
            foreach (var titem in BuiltStep.limits)
            {
                ListViewItem oneparam = new ListViewItem(titem.name);
                oneparam.SubItems.Add(titem.value1);
                oneparam.SubItems.Add(titem.value2);
                lvLimits.Items.Add(oneparam);
            }
            iform.Dispose();
       }

        private void bDeleteParameter_Click(object sender, EventArgs e)
        {
            if (lvParameter.SelectedItems.Count != 0)
            {
                if (BuiltStep.parameters[lvParameter.SelectedIndices[0]].optional)
                {
                    if (MessageBox.Show("Delete parameter " + lvParameter.SelectedItems[0].Text + "?", "Parameter Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        lvParameter.Items[lvParameter.SelectedIndices[0]].Remove();
                        BuiltStep.parameters.Clear();           // rebuilt the table
                        foreach (ListViewItem item in lvParameter.Items)
                        {
                            LimitsData.parametersItem lclpara = new LimitsData.parametersItem();
                            lclpara.name = item.Text;
                            lclpara.value = item.SubItems[1].Text;
                            BuiltStep.parameters.Add(lclpara);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("This is not a optional item. It can not be deleted.");
                }
            }
        }

        private void bDeleteLimit_Click(object sender, EventArgs e)
        {
            if (lvLimits.SelectedItems.Count != 0)
            {
                if (BuiltStep.limits[lvLimits.SelectedIndices[0]].optional)
                {
                    if (MessageBox.Show("Delete limit " + lvLimits.SelectedItems[0].Text + "?", "Limit Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        BuiltStep.limits.RemoveAt(BuiltStep.limits.FindIndex(x => x.name == lvLimits.SelectedItems[0].Text));
                        lvLimits.Items[lvLimits.SelectedIndices[0]].Remove();
                    }

                }
                else
                {
                    MessageBox.Show("This is not a optional item. It can not be deleted.");
                }
            }
        }

        private void bEditParameter_Click(object sender, EventArgs e)
        {
            if (lvParameter.SelectedIndices.Count == 0)
                return;
            ParameterSetup iform = new ParameterSetup(BuiltStep.parameters[lvParameter.SelectedIndices[0]]);
            iform.ShowDialog();

            BuiltStep.parameters.RemoveAt(lvParameter.SelectedIndices[0]);
            BuiltStep.parameters.Add(iform.ParamList[0]);
            lvParameter.Items.Clear();
            foreach (var titem in BuiltStep.parameters)
            {
                ListViewItem oneparam = new ListViewItem(titem.name);
                oneparam.SubItems.Add(titem.value);
                lvParameter.Items.Add(oneparam);
            }
            iform.Dispose();

        }

        private void bEditLimit_Click(object sender, EventArgs e)
        {
            LimitsData.limitsItem tempLimit = BuiltStep.limits[lvLimits.SelectedIndices[0]];
            LimitsSetup iform = new LimitsSetup(tempLimit);
            if (iform.ShowDialog() == DialogResult.OK)
            {
                BuiltStep.limits.RemoveAt(lvLimits.SelectedIndices[0]);
                BuiltStep.limits.Add(iform.LimitList[0]);
                lvLimits.Items.Clear();
                foreach (var titem in BuiltStep.limits)
                {
                    ListViewItem oneparam = new ListViewItem(titem.name);
                    oneparam.SubItems.Add(titem.value1);
                    oneparam.SubItems.Add(titem.value2);
                    oneparam.SubItems.Add(titem.operation);
                    lvLimits.Items.Add(oneparam);
                }
            }

            iform.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void tbStepName_TextChanged(object sender, EventArgs e)
        {
            BuiltStep.name = tbStepName.Text;
        }
    }
}
