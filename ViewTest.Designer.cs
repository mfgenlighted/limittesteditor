﻿namespace TestEditor
{
    partial class ViewSteps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lFunction = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lvGlobals = new System.Windows.Forms.ListView();
            this.global_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.global_value = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.bView = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.lvStepData = new System.Windows.Forms.ListView();
            this.cStepNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cStepName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cFunctionCall = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.lVersion = new System.Windows.Forms.Label();
            this.lProductCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lFunction
            // 
            this.lFunction.AutoSize = true;
            this.lFunction.Location = new System.Drawing.Point(169, 44);
            this.lFunction.Name = "lFunction";
            this.lFunction.Size = new System.Drawing.Size(19, 13);
            this.lFunction.TabIndex = 42;
            this.lFunction.Text = "----";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(169, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Function";
            // 
            // lvGlobals
            // 
            this.lvGlobals.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.global_name,
            this.global_value});
            this.lvGlobals.FullRowSelect = true;
            this.lvGlobals.GridLines = true;
            this.lvGlobals.Location = new System.Drawing.Point(707, 100);
            this.lvGlobals.Name = "lvGlobals";
            this.lvGlobals.Size = new System.Drawing.Size(289, 206);
            this.lvGlobals.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvGlobals.TabIndex = 37;
            this.lvGlobals.UseCompatibleStateImageBehavior = false;
            this.lvGlobals.View = System.Windows.Forms.View.Details;
            // 
            // global_name
            // 
            this.global_name.Text = "Name";
            this.global_name.Width = 82;
            // 
            // global_value
            // 
            this.global_value.Text = "Value";
            this.global_value.Width = 195;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(707, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Globals";
            // 
            // bView
            // 
            this.bView.Location = new System.Drawing.Point(557, 152);
            this.bView.Name = "bView";
            this.bView.Size = new System.Drawing.Size(75, 23);
            this.bView.TabIndex = 35;
            this.bView.Text = "View Step";
            this.bView.UseVisualStyleBackColor = true;
            this.bView.Click += new System.EventHandler(this.bView_Click);
            // 
            // bSave
            // 
            this.bSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bSave.Location = new System.Drawing.Point(707, 443);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 28);
            this.bSave.TabIndex = 31;
            this.bSave.Text = "Exit";
            this.bSave.UseVisualStyleBackColor = true;
            // 
            // lvStepData
            // 
            this.lvStepData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cStepNumber,
            this.cStepName,
            this.cFunctionCall});
            this.lvStepData.FullRowSelect = true;
            this.lvStepData.GridLines = true;
            this.lvStepData.Location = new System.Drawing.Point(28, 91);
            this.lvStepData.Name = "lvStepData";
            this.lvStepData.Size = new System.Drawing.Size(503, 465);
            this.lvStepData.TabIndex = 27;
            this.lvStepData.UseCompatibleStateImageBehavior = false;
            this.lvStepData.View = System.Windows.Forms.View.Details;
            // 
            // cStepNumber
            // 
            this.cStepNumber.Text = "Step Number";
            this.cStepNumber.Width = 82;
            // 
            // cStepName
            // 
            this.cStepName.Text = "Step Name";
            this.cStepName.Width = 195;
            // 
            // cFunctionCall
            // 
            this.cFunctionCall.Text = "Function Call";
            this.cFunctionCall.Width = 222;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Steps";
            // 
            // lVersion
            // 
            this.lVersion.AutoSize = true;
            this.lVersion.Location = new System.Drawing.Point(102, 44);
            this.lVersion.Name = "lVersion";
            this.lVersion.Size = new System.Drawing.Size(19, 13);
            this.lVersion.TabIndex = 25;
            this.lVersion.Text = "----";
            // 
            // lProductCode
            // 
            this.lProductCode.AutoSize = true;
            this.lProductCode.Location = new System.Drawing.Point(25, 44);
            this.lProductCode.Name = "lProductCode";
            this.lProductCode.Size = new System.Drawing.Size(19, 13);
            this.lProductCode.TabIndex = 24;
            this.lProductCode.Text = "----";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Version";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Product code";
            // 
            // ViewSteps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 628);
            this.Controls.Add(this.lFunction);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lvGlobals);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bView);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.lvStepData);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lVersion);
            this.Controls.Add(this.lProductCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ViewSteps";
            this.Text = "ViewStep";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lFunction;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListView lvGlobals;
        private System.Windows.Forms.ColumnHeader global_name;
        private System.Windows.Forms.ColumnHeader global_value;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bView;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.ListView lvStepData;
        private System.Windows.Forms.ColumnHeader cStepNumber;
        private System.Windows.Forms.ColumnHeader cStepName;
        private System.Windows.Forms.ColumnHeader cFunctionCall;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lVersion;
        private System.Windows.Forms.Label lProductCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}