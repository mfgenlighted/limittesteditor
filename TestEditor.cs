﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Reflection;

using LimitsDatabase;

namespace TestEditor
{
    public struct serverDataItem
    {
        public string name;
        public string ip;
        public string user;
        public string password;
        public string database;
        public string availTestDB;
    }


    public partial class TestEditor : Form
    {
        private LimitsData limitsDB;
        string pcFilter = string.Empty;             // product code filter for the test list
        string functionFilter = string.Empty;       // function code filter
        serverDataItem serverInfo = new serverDataItem();
        public bool EditDataMode = true;

        public TestEditor()
        {
            string availTestDBToUse = string.Empty;

            InitializeComponent();
            lEditingOn.Text = Properties.Settings.Default.CurrentServerIP + ":" + Properties.Settings.Default.LimitsDatabase;
        }

        private void configureToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ServerSetup iform = new ServerSetup();
            iform.ShowDialog();
            iform.Dispose();
            lEditingOn.Text = Properties.Settings.Default.CurrentServerIP + ":" + Properties.Settings.Default.LimitsDatabase;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void bGetCurrentLimits_Click(object sender, EventArgs e)
        {
            List<string> productList = new List<string>();
            string outmsg = string.Empty;
            ListViewItem lvpc = new ListViewItem();

            SelectServer sform = new SelectServer();
            sform.ShowDialog();
            serverInfo = sform.selectedServerData;
            lEditingOn.Text = serverInfo.name + ":" + Properties.Settings.Default.LimitsDatabase;
            limitsDB = new LimitsData(serverInfo.ip, serverInfo.database, serverInfo.user, serverInfo.password, serverInfo.availTestDB);



            // get a list of existing limits
            limitsDB.GetListOfProductCodes(ref productList, ref outmsg);
            lvAllProducts.Items.Clear();
            GetTestListFilter iform = new GetTestListFilter(pcFilter, functionFilter);
            iform.ShowDialog();
            pcFilter = iform.PCFilter;
            functionFilter = iform.FunctionFilter;
            if (pcFilter == string.Empty)
                lbFilter.Text = "none";
            else
                lbFilter.Text = pcFilter;
            if (functionFilter != string.Empty)
                lbFilter.Text  = lbFilter.Text + ":" + functionFilter;
            iform.Dispose();

            rewriteTestList();
        }


        private void bEdit_Click(object sender, EventArgs e)
        {
            EditProductCodeVersion();
        }

        private void bNew_Click(object sender, EventArgs e)
        {
            ListViewItem lvpc = new ListViewItem();
            List<string> productList = new List<string>();
            string outmsg = string.Empty;

            NewProductCodeVersion();
            // get a list of existing limits
            limitsDB.GetListOfProductCodes(ref productList, ref outmsg);

            rewriteTestList();
        }


        /// <summary>
        /// Make a new product code
        /// </summary>
        private void NewProductCodeVersion()
        {
            List<string> productList = new List<string>();
            string outmsg = string.Empty;
            string selectedProductAndVersionAndFunction = string.Empty;   // data from the operator prompt 
            string selectedProductAndFunction = string.Empty;          // data from the operator prompt
            string selectedProductcode = string.Empty;              // data from the operator prompt
            string selectedVersion = string.Empty;                  // data from the operator prompt
            string selectedFunction = string.Empty;                 // data from the operator prompt
            LimitsData.product_codesItem codeData = new LimitsData.product_codesItem();

            // get a list of existing limits
            limitsDB.GetListOfProductCodes(ref productList, ref outmsg);

            // get the product code/ version
            GetProductCode iform = new GetProductCode();
            iform.ShowDialog();
            selectedProductAndVersionAndFunction = iform.ProductCode + ":" + iform.Version + ":" + iform.FunctionCode;
            selectedProductAndFunction = iform.ProductCode  + ":" + iform.FunctionCode;
            selectedProductcode = iform.ProductCode;
            selectedFunction = iform.FunctionCode;
            iform.Dispose();

            // make sure product/function does not exists already
            foreach (var item in productList)
            {
                string[] parts = item.Split('#');
                if ((parts[0] == selectedProductcode) & (parts[2] == selectedFunction))
                {
                    MessageBox.Show("Product code/Function code exists already.");
                    return;
                }
            }

            var yn = MessageBox.Show("Make new limits from existing?", "New Limits", MessageBoxButtons.YesNo);
            if (yn == DialogResult.No)
            {
                // get the of the data
                AddSteps lform = new AddSteps(limitsDB, selectedProductAndVersionAndFunction);
                lform.ShowDialog();
                lform.Dispose();
            }
            else
            {
                PickProductCodeAndVersion pform = new PickProductCodeAndVersion(limitsDB);
                pform.ShowDialog();
                selectedProductcode = pform.PickedProductCode;
                selectedVersion = pform.PickedVersion;
                pform.Dispose();

                AddSteps sform = new AddSteps(limitsDB, selectedProductAndVersionAndFunction, selectedProductcode + ":" + selectedVersion + ":" + selectedFunction);
                sform.ShowDialog();
                sform.Dispose();
                // get new product code/version to record it under
                // run task to make new copy
            }
        }

        /// <summary>
        /// Edit a current test. It will add 0.01 to the version.
        /// </summary>
        private void EditProductCodeVersion()
        {
            List<string> productList = new List<string>();
            string outmsg = string.Empty;
            if (lvAllProducts.SelectedItems.Count == 0)
                return;

            string selectedProductcode = lvAllProducts.SelectedItems[0].Text;
            string selectedVersion = lvAllProducts.SelectedItems[0].SubItems[1].Text;
            string selectedFunction = lvAllProducts.SelectedItems[0].SubItems[2].Text;
            string newVersion = (float.Parse(lvAllProducts.SelectedItems[0].SubItems[1].Text) + 0.01).ToString("N2");
            float fversion = 0;
            if (MessageBox.Show("Do you want to incerment the major version?", "Version", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                newVersion = (Math.Truncate(float.Parse(lvAllProducts.SelectedItems[0].SubItems[1].Text)) + 1).ToString("N2");
            }

            // verify the following
            //  productCode/Version do not exist
            //  there is now productCode with a higher version
            if (!limitsDB.GetListOfProductCodes(ref productList, ref outmsg))
            {
                MessageBox.Show("Error geting the product list. " + outmsg);
                DialogResult = DialogResult.Cancel;
                return;
            }
            foreach (var item in productList)
            {
                string[] parts = item.Split('#');
                if ((selectedProductcode + ":" + newVersion + ":" + selectedFunction) == item)        // if the productcode/version exists
                {
                    MessageBox.Show("The new product code/version(" + selectedProductcode + ":" + selectedVersion + ":" + selectedFunction + ") exists already.");
                    return;
                }
                if ((selectedProductcode == parts[0]) & (selectedFunction == parts[2]))    // if the right product code/function code
                {
                    if (!float.TryParse(parts[1], out fversion))
                    {
                        MessageBox.Show("Error converting version number(" + parts[1] + ". Must be a float.");
                        return;
                    }
                    if (fversion > float.Parse(selectedVersion))        // if there is a greater version
                    {
                        MessageBox.Show("Can not  edit a lower version. A higher version exists");
                        return;
                    }
                }
            }


            AddSteps sform = new AddSteps(limitsDB, selectedProductcode + ":" + newVersion + ":" + selectedFunction, selectedProductcode + ":" + selectedVersion + ":" + selectedFunction);
            sform.ShowDialog();
            sform.Dispose();
            // get new product code/version to record it under
            // run task to make new copy
        }

        private void exportDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestExporter testexporter = new TestExporter();
            testexporter.ShowDialog();
            testexporter.Dispose();
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            string error = string.Empty;

            if (lvAllProducts.SelectedItems.Count == 0)
                return;
            if (MessageBox.Show("Are you sure you want to delete this limit?", "", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            limitsDB.DeleteData(lvAllProducts.SelectedItems[0].Text, lvAllProducts.SelectedItems[0].SubItems[1].Text, lvAllProducts.SelectedItems[0].SubItems[2].Text, ref error);
            rewriteTestList();
        }

        private void importDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestImporter iform = new TestImporter();
            iform.ShowDialog();
            iform.Dispose();
        }

        private void rewriteTestList()
        {
            List<string> productList = new List<string>();
            string outmsg = string.Empty;
            ListViewItem lvpc = new ListViewItem();

            // get a list of existing limits
            limitsDB.GetListOfProductCodes(ref productList, ref outmsg);
            lvAllProducts.Items.Clear();

            string filter = string.Empty;
            // data has the format <product code>#<version>#<function code>#<modify date>
            if ((pcFilter != string.Empty) | (functionFilter != string.Empty))
            {
                if (pcFilter != string.Empty)
                {
                    filter = pcFilter;
                }
                if (functionFilter != string.Empty)
                {
                    if (filter == string.Empty)
                    {
                        filter = ".*#.*#" + functionFilter;
                    }
                    else
                    {
                        filter = filter + "#.*#" + functionFilter;
                    }
                }
            }
            foreach (var item in productList)
            {
                if (filter != string.Empty)
                {
                    Match match = Regex.Match(item, filter);
                    if (match.Success)
                    {
                        string[] parts = item.Split('#');
                        lvpc = new ListViewItem(parts[0]);
                        lvpc.SubItems.Add(parts[1]);
                        lvpc.SubItems.Add(parts[2]);
                        lvpc.SubItems.Add(parts[3]);
                        lvAllProducts.Items.Add(lvpc);
                    }
                }
                else
                {
                    string[] parts = item.Split('#');
                    lvpc = new ListViewItem(parts[0]);
                    lvpc.SubItems.Add(parts[1]);
                    lvpc.SubItems.Add(parts[2]);
                    lvpc.SubItems.Add(parts[3]);
                    lvAllProducts.Items.Add(lvpc);
                }
            }

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string textProgramVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            MessageBox.Show("Program version: " + textProgramVersion);
        }

        private void rbPacket_CheckedChanged(object sender, EventArgs e)
        {
            string availTestDBToUse = string.Empty;

            RadioButton who = (RadioButton)sender;
            if (who.Name == "rbPacket")
                availTestDBToUse = Properties.Settings.Default.PacketSensorAvailDatabase;
            if (who.Name == "rbText")
                availTestDBToUse = Properties.Settings.Default.TextSensorAvailDatabase;
            if (who.Name == "rbCU")
                availTestDBToUse = Properties.Settings.Default.CUAvailDatabase;
            limitsDB = new LimitsData(Properties.Settings.Default.CurrentServerIP, Properties.Settings.Default.LimitsDatabase, Properties.Settings.Default.CurrentUser,
                                                                                                            Properties.Settings.Default.CurrentPassword, availTestDBToUse);
        }

        private void bView_Click(object sender, EventArgs e)
        {
            string selectedProductcode = lvAllProducts.SelectedItems[0].Text;
            string selectedVersion = lvAllProducts.SelectedItems[0].SubItems[1].Text;
            string selectedFunction = lvAllProducts.SelectedItems[0].SubItems[2].Text;
            string newVersion = lvAllProducts.SelectedItems[0].SubItems[1].Text;
            EditDataMode = false;

            ViewSteps sform = new ViewSteps(limitsDB, selectedProductcode + ":" + selectedVersion + ":" + selectedFunction);
            sform.ShowDialog();
            sform.Dispose();

        }
    }
}
