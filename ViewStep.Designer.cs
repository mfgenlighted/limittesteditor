﻿namespace TestEditor
{
    partial class ViewStep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.tbStepName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbStepOperation = new System.Windows.Forms.ComboBox();
            this.lvLimits = new System.Windows.Forms.ListView();
            this.limitName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.limitValue1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.limitValue2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.limitOperation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvParameter = new System.Windows.Forms.ListView();
            this.parameterName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.parameterData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bSave = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.nudPassAfterxPasses = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nudMaxFails = new System.Windows.Forms.NumericUpDown();
            this.cbTestType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbActionOnPass = new System.Windows.Forms.ComboBox();
            this.cbActionOnFail = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudPassAfterxPasses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxFails)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(374, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Test name";
            // 
            // tbStepName
            // 
            this.tbStepName.Enabled = false;
            this.tbStepName.Location = new System.Drawing.Point(374, 33);
            this.tbStepName.Name = "tbStepName";
            this.tbStepName.Size = new System.Drawing.Size(375, 20);
            this.tbStepName.TabIndex = 50;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Location = new System.Drawing.Point(73, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Step Operation";
            // 
            // cbStepOperation
            // 
            this.cbStepOperation.Enabled = false;
            this.cbStepOperation.FormattingEnabled = true;
            this.cbStepOperation.Items.AddRange(new object[] {
            "RUN",
            "SKIP",
            "PASS",
            "FAIL",
            "GOTO"});
            this.cbStepOperation.Location = new System.Drawing.Point(73, 215);
            this.cbStepOperation.Name = "cbStepOperation";
            this.cbStepOperation.Size = new System.Drawing.Size(77, 21);
            this.cbStepOperation.TabIndex = 46;
            this.cbStepOperation.Text = "RUN";
            // 
            // lvLimits
            // 
            this.lvLimits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.limitName,
            this.limitValue1,
            this.limitValue2,
            this.limitOperation});
            this.lvLimits.GridLines = true;
            this.lvLimits.Location = new System.Drawing.Point(767, 112);
            this.lvLimits.Name = "lvLimits";
            this.lvLimits.Size = new System.Drawing.Size(377, 230);
            this.lvLimits.TabIndex = 43;
            this.lvLimits.UseCompatibleStateImageBehavior = false;
            this.lvLimits.View = System.Windows.Forms.View.Details;
            // 
            // limitName
            // 
            this.limitName.Text = "Name";
            this.limitName.Width = 107;
            // 
            // limitValue1
            // 
            this.limitValue1.Text = "Value1";
            this.limitValue1.Width = 93;
            // 
            // limitValue2
            // 
            this.limitValue2.Text = "Value2";
            this.limitValue2.Width = 109;
            // 
            // limitOperation
            // 
            this.limitOperation.Text = "Operation";
            // 
            // lvParameter
            // 
            this.lvParameter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.parameterName,
            this.parameterData});
            this.lvParameter.GridLines = true;
            this.lvParameter.Location = new System.Drawing.Point(374, 112);
            this.lvParameter.Name = "lvParameter";
            this.lvParameter.Size = new System.Drawing.Size(318, 230);
            this.lvParameter.TabIndex = 42;
            this.lvParameter.UseCompatibleStateImageBehavior = false;
            this.lvParameter.View = System.Windows.Forms.View.Details;
            // 
            // parameterName
            // 
            this.parameterName.Text = "Name";
            this.parameterName.Width = 107;
            // 
            // parameterData
            // 
            this.parameterData.Text = "Data";
            this.parameterData.Width = 203;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(764, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Limits";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(371, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Parameters";
            // 
            // bSave
            // 
            this.bSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bSave.Location = new System.Drawing.Point(1058, 398);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 34;
            this.bSave.Text = "Exit";
            this.bSave.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(182, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Pass after x passes";
            // 
            // nudPassAfterxPasses
            // 
            this.nudPassAfterxPasses.Enabled = false;
            this.nudPassAfterxPasses.Location = new System.Drawing.Point(179, 153);
            this.nudPassAfterxPasses.Name = "nudPassAfterxPasses";
            this.nudPassAfterxPasses.Size = new System.Drawing.Size(68, 20);
            this.nudPassAfterxPasses.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(79, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Max fails";
            // 
            // nudMaxFails
            // 
            this.nudMaxFails.Enabled = false;
            this.nudMaxFails.Location = new System.Drawing.Point(76, 153);
            this.nudMaxFails.Name = "nudMaxFails";
            this.nudMaxFails.Size = new System.Drawing.Size(68, 20);
            this.nudMaxFails.TabIndex = 32;
            // 
            // cbTestType
            // 
            this.cbTestType.Enabled = false;
            this.cbTestType.FormattingEnabled = true;
            this.cbTestType.Items.AddRange(new object[] {
            "RUN",
            "PASSFAIL",
            "LIMITS"});
            this.cbTestType.Location = new System.Drawing.Point(73, 32);
            this.cbTestType.Name = "cbTestType";
            this.cbTestType.Size = new System.Drawing.Size(77, 21);
            this.cbTestType.TabIndex = 26;
            this.cbTestType.Text = "RUN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(167, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Action on Pass";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(73, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Action on Fail";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(70, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Test type";
            // 
            // cbActionOnPass
            // 
            this.cbActionOnPass.Enabled = false;
            this.cbActionOnPass.FormattingEnabled = true;
            this.cbActionOnPass.Items.AddRange(new object[] {
            "NEXT",
            "END"});
            this.cbActionOnPass.Location = new System.Drawing.Point(170, 96);
            this.cbActionOnPass.Name = "cbActionOnPass";
            this.cbActionOnPass.Size = new System.Drawing.Size(77, 21);
            this.cbActionOnPass.TabIndex = 30;
            this.cbActionOnPass.Text = "NEXT";
            // 
            // cbActionOnFail
            // 
            this.cbActionOnFail.Enabled = false;
            this.cbActionOnFail.FormattingEnabled = true;
            this.cbActionOnFail.Items.AddRange(new object[] {
            "NEXT",
            "END",
            "RETEST"});
            this.cbActionOnFail.Location = new System.Drawing.Point(73, 96);
            this.cbActionOnFail.Name = "cbActionOnFail";
            this.cbActionOnFail.Size = new System.Drawing.Size(77, 21);
            this.cbActionOnFail.TabIndex = 27;
            this.cbActionOnFail.Text = "END";
            // 
            // SetView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1231, 472);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbStepName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbStepOperation);
            this.Controls.Add(this.lvLimits);
            this.Controls.Add(this.lvParameter);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nudPassAfterxPasses);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudMaxFails);
            this.Controls.Add(this.cbTestType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbActionOnPass);
            this.Controls.Add(this.cbActionOnFail);
            this.Name = "SetView";
            this.Text = "SetView";
            ((System.ComponentModel.ISupportInitialize)(this.nudPassAfterxPasses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxFails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbStepName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbStepOperation;
        private System.Windows.Forms.ListView lvLimits;
        private System.Windows.Forms.ColumnHeader limitName;
        private System.Windows.Forms.ColumnHeader limitValue1;
        private System.Windows.Forms.ColumnHeader limitValue2;
        private System.Windows.Forms.ColumnHeader limitOperation;
        private System.Windows.Forms.ListView lvParameter;
        private System.Windows.Forms.ColumnHeader parameterName;
        private System.Windows.Forms.ColumnHeader parameterData;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudPassAfterxPasses;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudMaxFails;
        private System.Windows.Forms.ComboBox cbTestType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbActionOnPass;
        private System.Windows.Forms.ComboBox cbActionOnFail;
    }
}