﻿namespace TestEditor
{
    partial class TestImporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.bDone = new System.Windows.Forms.Button();
            this.bStartImport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lImportingTo = new System.Windows.Forms.Label();
            this.rtbFileList = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Select import file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(513, 68);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 1;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            // 
            // bStartImport
            // 
            this.bStartImport.Location = new System.Drawing.Point(459, 39);
            this.bStartImport.Name = "bStartImport";
            this.bStartImport.Size = new System.Drawing.Size(75, 23);
            this.bStartImport.TabIndex = 3;
            this.bStartImport.Text = "Start Import";
            this.bStartImport.UseVisualStyleBackColor = true;
            this.bStartImport.Click += new System.EventHandler(this.bStartImport_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Importing to";
            // 
            // lImportingTo
            // 
            this.lImportingTo.AutoSize = true;
            this.lImportingTo.Location = new System.Drawing.Point(44, 30);
            this.lImportingTo.Name = "lImportingTo";
            this.lImportingTo.Size = new System.Drawing.Size(19, 13);
            this.lImportingTo.TabIndex = 5;
            this.lImportingTo.Text = "----";
            // 
            // rtbFileList
            // 
            this.rtbFileList.Location = new System.Drawing.Point(26, 123);
            this.rtbFileList.Name = "rtbFileList";
            this.rtbFileList.Size = new System.Drawing.Size(362, 206);
            this.rtbFileList.TabIndex = 6;
            this.rtbFileList.Text = "";
            // 
            // TestImporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 364);
            this.Controls.Add(this.rtbFileList);
            this.Controls.Add(this.lImportingTo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bStartImport);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.button1);
            this.Name = "TestImporter";
            this.Text = "TestImporter";
            this.Load += new System.EventHandler(this.TestImporter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.Button bStartImport;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lImportingTo;
        private System.Windows.Forms.RichTextBox rtbFileList;
    }
}