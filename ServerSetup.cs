﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LimitsDatabase;

namespace TestEditor
{
    public partial class ServerSetup : Form
    {
        public ServerSetup()
        {
            InitializeComponent();
        }

        private void ServerSetup_Load(object sender, EventArgs e)
        {
            tbS1Name.Text = Properties.Settings.Default.S1Name;
            tbS1IP.Text = Properties.Settings.Default.S1IP;
            tbS1User.Text = Properties.Settings.Default.S1User;
            tbS1Password.Text = Properties.Settings.Default.S1Password;
            tbS2Name.Text = Properties.Settings.Default.S2Name;
            tbS2IP.Text = Properties.Settings.Default.S2IP;
            tbS2User.Text = Properties.Settings.Default.S2User;
            tbS2Password.Text = Properties.Settings.Default.S2Password;
            tbS3Name.Text = Properties.Settings.Default.S3Name;
            tbS3IP.Text = Properties.Settings.Default.S3IP;
            tbS3User.Text = Properties.Settings.Default.S3User;
            tbS3Password.Text = Properties.Settings.Default.S3Password;
            tbS4Name.Text = Properties.Settings.Default.S4Name;
            tbS4IP.Text = Properties.Settings.Default.S4IP;
            tbS4User.Text = Properties.Settings.Default.S4User;
            tbS4Password.Text = Properties.Settings.Default.S4Password;

            tbLimitsDatabase.Text = Properties.Settings.Default.LimitsDatabase;
            tbPacketAvailDB.Text = Properties.Settings.Default.PacketSensorAvailDatabase;
            tbTextAvailDB.Text = Properties.Settings.Default.TextSensorAvailDatabase;
            tbCUAvailDB.Text = Properties.Settings.Default.CUAvailDatabase;
            tbImportTransferDir.Text = Properties.Settings.Default.TransferDir;

        }

        private void bDone_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.S1Name = tbS1Name.Text; 
            Properties.Settings.Default.S1IP =  tbS1IP.Text; 
            Properties.Settings.Default.S1User = tbS1User.Text; 
            Properties.Settings.Default.S1Password = tbS1Password.Text;
            Properties.Settings.Default.S2Name = tbS2Name.Text; 
            Properties.Settings.Default.S2IP =  tbS2IP.Text; 
            Properties.Settings.Default.S2User = tbS2User.Text;
            Properties.Settings.Default.S2Password =  tbS2Password.Text; 
            Properties.Settings.Default.S3Name = tbS3Name.Text; 
            Properties.Settings.Default.S3IP = tbS3IP.Text;
            Properties.Settings.Default.S3User = tbS3User.Text; 
            Properties.Settings.Default.S3Password = tbS3Password.Text; 
            Properties.Settings.Default.S4Name = tbS4Name.Text; 
            Properties.Settings.Default.S4IP = tbS4IP.Text; 
            Properties.Settings.Default.S4User = tbS4User.Text; 
            Properties.Settings.Default.S4Password = tbS4Password.Text; 

            Properties.Settings.Default.LimitsDatabase = tbLimitsDatabase.Text; 
            Properties.Settings.Default.PacketSensorAvailDatabase = tbPacketAvailDB.Text; 
            Properties.Settings.Default.TextSensorAvailDatabase = tbTextAvailDB.Text; 
            Properties.Settings.Default.CUAvailDatabase = tbCUAvailDB.Text; 
            Properties.Settings.Default.TransferDir = tbImportTransferDir.Text;

            Properties.Settings.Default.Save();
        }

        private void bSever1Test_Click(object sender, EventArgs e)
        {
            LimitsData lDB = new LimitsData(tbS1IP.Text, tbLimitsDatabase.Text, tbS1User.Text, tbS1Password.Text, tbPacketAvailDB.Text);
            lDB.OpenLimitsData();
            string error = lDB.LastErrorMessage;
            if (error == string.Empty)
                MessageBox.Show("Server info good");
            else
                MessageBox.Show("Error: " + error);
        }
        private void bSever2Test_Click(object sender, EventArgs e)
        {
            LimitsData lDB = new LimitsData(tbS2IP.Text, tbLimitsDatabase.Text, tbS2User.Text, tbS2Password.Text, tbPacketAvailDB.Text);
            lDB.OpenLimitsData();
            string error = lDB.LastErrorMessage;
            if (error == string.Empty)
                MessageBox.Show("Server info good");
            else
                MessageBox.Show("Error: " + error);
        }
        private void bSever3Test_Click(object sender, EventArgs e)
        {
            LimitsData lDB = new LimitsData(tbS3IP.Text, tbLimitsDatabase.Text, tbS3User.Text, tbS3Password.Text, tbPacketAvailDB.Text);
            lDB.OpenLimitsData();
            string error = lDB.LastErrorMessage;
            if (error == string.Empty)
                MessageBox.Show("Server info good");
            else
                MessageBox.Show("Error: " + error);
        }
        private void bSever4Test_Click(object sender, EventArgs e)
        {
            LimitsData lDB = new LimitsData(tbS4IP.Text, tbLimitsDatabase.Text, tbS4User.Text, tbS4Password.Text, tbPacketAvailDB.Text);
            lDB.OpenLimitsData();
            string error = lDB.LastErrorMessage;
            if (error == string.Empty)
                MessageBox.Show("Server info good");
            else
                MessageBox.Show("Error: " + error);
        }

        private void bImportBrowse_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Set directory to save SQL files";
            tbImportTransferDir.Text = Properties.Settings.Default.TransferDir;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbImportTransferDir.Text = folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.TransferDir = folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.Save();
            }

        }
    }
}
