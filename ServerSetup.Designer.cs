﻿namespace TestEditor
{
    partial class ServerSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bDone = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bServer4Test = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.tbS4Password = new System.Windows.Forms.TextBox();
            this.tbS4Name = new System.Windows.Forms.TextBox();
            this.tbS4IP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbS4User = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bServer3Test = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.tbS3Name = new System.Windows.Forms.TextBox();
            this.tbS3Password = new System.Windows.Forms.TextBox();
            this.tbS3IP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbS3User = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bServer2Test = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tbS2Password = new System.Windows.Forms.TextBox();
            this.tbS2Name = new System.Windows.Forms.TextBox();
            this.tbS2IP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbS2User = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbLimitsDatabase = new System.Windows.Forms.TextBox();
            this.server1 = new System.Windows.Forms.GroupBox();
            this.bSever1Test = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.tbS1Password = new System.Windows.Forms.TextBox();
            this.tbS1Name = new System.Windows.Forms.TextBox();
            this.tbS1IP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbS1User = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bImportBrowse = new System.Windows.Forms.Button();
            this.tbImportTransferDir = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbPacketAvailDB = new System.Windows.Forms.TextBox();
            this.tbTextAvailDB = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbCUAvailDB = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.server1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(544, 579);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 9;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bServer4Test);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.tbS4Password);
            this.groupBox3.Controls.Add(this.tbS4Name);
            this.groupBox3.Controls.Add(this.tbS4IP);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.tbS4User);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Location = new System.Drawing.Point(275, 259);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(202, 217);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "server 4";
            // 
            // bServer4Test
            // 
            this.bServer4Test.Location = new System.Drawing.Point(53, 188);
            this.bServer4Test.Name = "bServer4Test";
            this.bServer4Test.Size = new System.Drawing.Size(75, 23);
            this.bServer4Test.TabIndex = 14;
            this.bServer4Test.Text = "Test";
            this.bServer4Test.UseVisualStyleBackColor = true;
            this.bServer4Test.Click += new System.EventHandler(this.bSever4Test_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "Name";
            // 
            // tbS4Password
            // 
            this.tbS4Password.Location = new System.Drawing.Point(6, 157);
            this.tbS4Password.Name = "tbS4Password";
            this.tbS4Password.PasswordChar = '*';
            this.tbS4Password.Size = new System.Drawing.Size(167, 20);
            this.tbS4Password.TabIndex = 3;
            // 
            // tbS4Name
            // 
            this.tbS4Name.Location = new System.Drawing.Point(9, 35);
            this.tbS4Name.Name = "tbS4Name";
            this.tbS4Name.Size = new System.Drawing.Size(167, 20);
            this.tbS4Name.TabIndex = 0;
            // 
            // tbS4IP
            // 
            this.tbS4IP.Location = new System.Drawing.Point(6, 79);
            this.tbS4IP.Name = "tbS4IP";
            this.tbS4IP.Size = new System.Drawing.Size(167, 20);
            this.tbS4IP.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 141);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Password";
            // 
            // tbS4User
            // 
            this.tbS4User.Location = new System.Drawing.Point(6, 118);
            this.tbS4User.Name = "tbS4User";
            this.tbS4User.Size = new System.Drawing.Size(167, 20);
            this.tbS4User.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "User";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 60);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Server";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bServer3Test);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.tbS3Name);
            this.groupBox2.Controls.Add(this.tbS3Password);
            this.groupBox2.Controls.Add(this.tbS3IP);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.tbS3User);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(28, 259);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(202, 217);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "server 3";
            // 
            // bServer3Test
            // 
            this.bServer3Test.Location = new System.Drawing.Point(35, 188);
            this.bServer3Test.Name = "bServer3Test";
            this.bServer3Test.Size = new System.Drawing.Size(75, 23);
            this.bServer3Test.TabIndex = 14;
            this.bServer3Test.Text = "Test";
            this.bServer3Test.UseVisualStyleBackColor = true;
            this.bServer3Test.Click += new System.EventHandler(this.bSever3Test_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Name";
            // 
            // tbS3Name
            // 
            this.tbS3Name.Location = new System.Drawing.Point(9, 35);
            this.tbS3Name.Name = "tbS3Name";
            this.tbS3Name.Size = new System.Drawing.Size(167, 20);
            this.tbS3Name.TabIndex = 0;
            // 
            // tbS3Password
            // 
            this.tbS3Password.Location = new System.Drawing.Point(9, 157);
            this.tbS3Password.Name = "tbS3Password";
            this.tbS3Password.PasswordChar = '*';
            this.tbS3Password.Size = new System.Drawing.Size(167, 20);
            this.tbS3Password.TabIndex = 3;
            // 
            // tbS3IP
            // 
            this.tbS3IP.Location = new System.Drawing.Point(9, 79);
            this.tbS3IP.Name = "tbS3IP";
            this.tbS3IP.Size = new System.Drawing.Size(167, 20);
            this.tbS3IP.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Password";
            // 
            // tbS3User
            // 
            this.tbS3User.Location = new System.Drawing.Point(9, 118);
            this.tbS3User.Name = "tbS3User";
            this.tbS3User.Size = new System.Drawing.Size(167, 20);
            this.tbS3User.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "User";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Server";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bServer2Test);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.tbS2Password);
            this.groupBox1.Controls.Add(this.tbS2Name);
            this.groupBox1.Controls.Add(this.tbS2IP);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbS2User);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(275, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 214);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "server 2";
            // 
            // bServer2Test
            // 
            this.bServer2Test.Location = new System.Drawing.Point(53, 185);
            this.bServer2Test.Name = "bServer2Test";
            this.bServer2Test.Size = new System.Drawing.Size(75, 23);
            this.bServer2Test.TabIndex = 14;
            this.bServer2Test.Text = "Test";
            this.bServer2Test.UseVisualStyleBackColor = true;
            this.bServer2Test.Click += new System.EventHandler(this.bSever2Test_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Name";
            // 
            // tbS2Password
            // 
            this.tbS2Password.Location = new System.Drawing.Point(9, 153);
            this.tbS2Password.Name = "tbS2Password";
            this.tbS2Password.PasswordChar = '*';
            this.tbS2Password.Size = new System.Drawing.Size(167, 20);
            this.tbS2Password.TabIndex = 3;
            // 
            // tbS2Name
            // 
            this.tbS2Name.Location = new System.Drawing.Point(12, 33);
            this.tbS2Name.Name = "tbS2Name";
            this.tbS2Name.Size = new System.Drawing.Size(167, 20);
            this.tbS2Name.TabIndex = 0;
            // 
            // tbS2IP
            // 
            this.tbS2IP.Location = new System.Drawing.Point(9, 75);
            this.tbS2IP.Name = "tbS2IP";
            this.tbS2IP.Size = new System.Drawing.Size(167, 20);
            this.tbS2IP.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Password";
            // 
            // tbS2User
            // 
            this.tbS2User.Location = new System.Drawing.Point(9, 114);
            this.tbS2User.Name = "tbS2User";
            this.tbS2User.Size = new System.Drawing.Size(167, 20);
            this.tbS2User.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "User";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Server";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(499, 243);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Limits Database";
            // 
            // tbLimitsDatabase
            // 
            this.tbLimitsDatabase.Location = new System.Drawing.Point(499, 259);
            this.tbLimitsDatabase.Name = "tbLimitsDatabase";
            this.tbLimitsDatabase.Size = new System.Drawing.Size(167, 20);
            this.tbLimitsDatabase.TabIndex = 4;
            // 
            // server1
            // 
            this.server1.Controls.Add(this.bSever1Test);
            this.server1.Controls.Add(this.label19);
            this.server1.Controls.Add(this.tbS1Password);
            this.server1.Controls.Add(this.tbS1Name);
            this.server1.Controls.Add(this.tbS1IP);
            this.server1.Controls.Add(this.label3);
            this.server1.Controls.Add(this.tbS1User);
            this.server1.Controls.Add(this.label2);
            this.server1.Controls.Add(this.label1);
            this.server1.Location = new System.Drawing.Point(28, 26);
            this.server1.Name = "server1";
            this.server1.Size = new System.Drawing.Size(202, 214);
            this.server1.TabIndex = 5;
            this.server1.TabStop = false;
            this.server1.Text = "server 1";
            // 
            // bSever1Test
            // 
            this.bSever1Test.Location = new System.Drawing.Point(35, 185);
            this.bSever1Test.Name = "bSever1Test";
            this.bSever1Test.Size = new System.Drawing.Size(75, 23);
            this.bSever1Test.TabIndex = 13;
            this.bSever1Test.Text = "Test";
            this.bSever1Test.UseVisualStyleBackColor = true;
            this.bSever1Test.Click += new System.EventHandler(this.bSever1Test_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Name";
            // 
            // tbS1Password
            // 
            this.tbS1Password.Location = new System.Drawing.Point(6, 153);
            this.tbS1Password.Name = "tbS1Password";
            this.tbS1Password.PasswordChar = '*';
            this.tbS1Password.Size = new System.Drawing.Size(167, 20);
            this.tbS1Password.TabIndex = 3;
            // 
            // tbS1Name
            // 
            this.tbS1Name.Location = new System.Drawing.Point(6, 33);
            this.tbS1Name.Name = "tbS1Name";
            this.tbS1Name.Size = new System.Drawing.Size(167, 20);
            this.tbS1Name.TabIndex = 0;
            // 
            // tbS1IP
            // 
            this.tbS1IP.Location = new System.Drawing.Point(6, 75);
            this.tbS1IP.Name = "tbS1IP";
            this.tbS1IP.Size = new System.Drawing.Size(167, 20);
            this.tbS1IP.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Password";
            // 
            // tbS1User
            // 
            this.tbS1User.Location = new System.Drawing.Point(6, 114);
            this.tbS1User.Name = "tbS1User";
            this.tbS1User.Size = new System.Drawing.Size(167, 20);
            this.tbS1User.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "User";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Server";
            // 
            // bImportBrowse
            // 
            this.bImportBrowse.Location = new System.Drawing.Point(459, 504);
            this.bImportBrowse.Name = "bImportBrowse";
            this.bImportBrowse.Size = new System.Drawing.Size(50, 25);
            this.bImportBrowse.TabIndex = 40;
            this.bImportBrowse.Text = "Browse";
            this.bImportBrowse.UseVisualStyleBackColor = true;
            this.bImportBrowse.Click += new System.EventHandler(this.bImportBrowse_Click);
            // 
            // tbImportTransferDir
            // 
            this.tbImportTransferDir.Location = new System.Drawing.Point(33, 507);
            this.tbImportTransferDir.Name = "tbImportTransferDir";
            this.tbImportTransferDir.Size = new System.Drawing.Size(415, 20);
            this.tbImportTransferDir.TabIndex = 42;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(34, 491);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "Transfer Directory";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(499, 62);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(179, 13);
            this.label22.TabIndex = 43;
            this.label22.Text = "Packet Sensor Avail Tests database";
            // 
            // tbPacketAvailDB
            // 
            this.tbPacketAvailDB.Location = new System.Drawing.Point(502, 82);
            this.tbPacketAvailDB.Name = "tbPacketAvailDB";
            this.tbPacketAvailDB.Size = new System.Drawing.Size(164, 20);
            this.tbPacketAvailDB.TabIndex = 44;
            // 
            // tbTextAvailDB
            // 
            this.tbTextAvailDB.Location = new System.Drawing.Point(500, 144);
            this.tbTextAvailDB.Name = "tbTextAvailDB";
            this.tbTextAvailDB.Size = new System.Drawing.Size(166, 20);
            this.tbTextAvailDB.TabIndex = 46;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(497, 124);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(166, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Text Sensor Avail Tests database";
            // 
            // tbCUAvailDB
            // 
            this.tbCUAvailDB.Location = new System.Drawing.Point(498, 202);
            this.tbCUAvailDB.Name = "tbCUAvailDB";
            this.tbCUAvailDB.Size = new System.Drawing.Size(168, 20);
            this.tbCUAvailDB.TabIndex = 48;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(495, 182);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(124, 13);
            this.label24.TabIndex = 47;
            this.label24.Text = "CU Avail Tests database";
            // 
            // ServerSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 632);
            this.Controls.Add(this.tbCUAvailDB);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tbTextAvailDB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.tbPacketAvailDB);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.bImportBrowse);
            this.Controls.Add(this.tbLimitsDatabase);
            this.Controls.Add(this.tbImportTransferDir);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.server1);
            this.Name = "ServerSetup";
            this.Text = "ServerSetup";
            this.Load += new System.EventHandler(this.ServerSetup_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.server1.ResumeLayout(false);
            this.server1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbS4Password;
        private System.Windows.Forms.TextBox tbS4Name;
        private System.Windows.Forms.TextBox tbS4IP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbS4User;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbS3Name;
        private System.Windows.Forms.TextBox tbS3Password;
        private System.Windows.Forms.TextBox tbS3IP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbS3User;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbS2Password;
        private System.Windows.Forms.TextBox tbS2Name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbS2IP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbS2User;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbLimitsDatabase;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox server1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbS1Password;
        private System.Windows.Forms.TextBox tbS1Name;
        private System.Windows.Forms.TextBox tbS1IP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbS1User;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bImportBrowse;
        private System.Windows.Forms.TextBox tbImportTransferDir;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbPacketAvailDB;
        private System.Windows.Forms.TextBox tbTextAvailDB;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbCUAvailDB;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button bServer4Test;
        private System.Windows.Forms.Button bServer3Test;
        private System.Windows.Forms.Button bServer2Test;
        private System.Windows.Forms.Button bSever1Test;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}