﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using LimitsDatabase;

namespace TestEditor
{
    public partial class TestImporter : Form
    {
        private LimitsData limitsDB;
        string ip = string.Empty;
        string user = string.Empty;
        string password = string.Empty;
        string database = string.Empty;
        string availdatabase = string.Empty;
        List<string> filelist = new List<string>();

        public TestImporter()
        {
            InitializeComponent();
        }


        //------------ Utilitys
        private static int ExecuteCommand(string commnd, int timeout, ref string outputString, ref string errorString)
        {

            var pp = new ProcessStartInfo("cmd.exe", "/C" + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C:\\",
            };
            pp.RedirectStandardError = true;
            pp.RedirectStandardOutput = true;
            var process = Process.Start(pp);
            try
            {
                process.WaitForExit(timeout);
            }
            catch (Exception)
            {
                return -1;
            }
            //if (!process.HasExited)
            //{
            //    MessageBox.Show("Problem with process.");
            //    process.Kill();
            //    process.Close();
            //    return -1;
            //}
            using (StreamReader rdr = process.StandardOutput)
            {
                outputString = rdr.ReadToEnd();
            }
            using (StreamReader rdr = process.StandardError)
            {
                errorString = rdr.ReadToEnd();
            }
            process.Close();
            return 0;
        }


        //---------------keys
        private void button1_Click(object sender, EventArgs e)
        {

            openFileDialog1.InitialDirectory = Properties.Settings.Default.TransferDir;
            openFileDialog1.Multiselect = true;
            openFileDialog1.Title = "Select files to import";
            DialogResult result = openFileDialog1.ShowDialog();
            filelist.Clear();
            rtbFileList.Clear();
            if (result == DialogResult.OK)
            {
                foreach (var item in openFileDialog1.FileNames)
                {
                    filelist.Add("\"" + item + "\"");   // put ' in just in case there are spaces
                    rtbFileList.AppendText(Path.GetFileName(item) + "\n");
                }
            }
        }

        private void bStartImport_Click(object sender, EventArgs e)
        {
            string cmd = string.Empty;
            string outputString = string.Empty;
            string errorString = string.Empty;
            Color oldcolor;
            string oldtext;
            int i = 1;

            oldcolor = bStartImport.BackColor;
            bStartImport.BackColor = Color.Green;
            oldtext = bStartImport.Text;
            foreach (var item in filelist)
            {
                bStartImport.Text = "Working " + i++;
                this.Refresh();
                cmd = "mysql -t -h" + ip + " -u" + user + " -p" + password + " " + database + " < " + item;
                ExecuteCommand(cmd, 10000, ref outputString, ref errorString);
            }
            bStartImport.BackColor = oldcolor;
            bStartImport.Text = oldtext;
            if (errorString != string.Empty)
            {
                if (!errorString.Contains("Warning"))   // ignore the warning
                {
                    MessageBox.Show("Error making product codes. " + errorString);
                    return;
                }
            }

        }

        private void TestImporter_Load(object sender, EventArgs e)
        {
            SelectServer iform = new SelectServer();
            iform.Text = "Select server to import to";
            iform.ShowDialog();
            serverDataItem selected = iform.selectedServerData;
            lImportingTo.Text = selected.name  + ":" + selected.database;
            limitsDB = new LimitsData(selected.ip, selected.database, selected.user, selected.password, selected.availTestDB);
            ip = selected.ip;
            user = selected.user;
            password = selected.password;
            database = selected.database;
        }
    }
}
